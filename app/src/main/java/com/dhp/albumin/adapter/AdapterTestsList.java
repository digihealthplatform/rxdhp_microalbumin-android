package com.dhp.albumin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TableTest;
import com.dhp.albumin.ui.patient.ActivityPatientTestDetails;

import static com.dhp.albumin.utils.LUtils.getFormattedDateTime;

public class AdapterTestsList extends RecyclerView.Adapter<AdapterTestsList.ViewHolder> {

    private final int patientId;

    private List<TableTest> tableTests;

    private final Context context;
    private final LayoutInflater layoutInflater;
    private final DataManager dataManager;

    public AdapterTestsList(Context context, DataManager dataManager, int patientId) {
        this.context = context;
        this.dataManager = dataManager;
        this.patientId = patientId;
        this.tableTests = dataManager.getAllTests(patientId);
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterTestsList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_test, parent, false);
        return new AdapterTestsList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterTestsList.ViewHolder holder, int position) {
        TableTest tableTest = tableTests.get(position);
        holder.tvTestType.setText(tableTest.getTestType());
        holder.tvTestDate.setText(getFormattedDateTime(tableTest.getTestDateTime()));
        holder.tvTestDiagnosis.setText(tableTest.getDiagnosis());
    }

    @Override
    public int getItemCount() {
        if (null != tableTests) {
            return tableTests.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTestType;
        private TextView tvTestDate;
        private TextView tvTestDiagnosis;

        ViewHolder(View itemView) {
            super(itemView);
            tvTestType = (TextView) itemView.findViewById(R.id.tv_test_type);
            tvTestDate = (TextView) itemView.findViewById(R.id.tv_test_date);
            tvTestDiagnosis = (TextView) itemView.findViewById(R.id.tv_test_values);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityPatientTestDetails.class);
            intent.putExtra("test_id", tableTests.get(getAdapterPosition()).getTestId());
            intent.putExtra("patient_id", tableTests.get(getAdapterPosition()).getPatientId());
            context.startActivity(intent);
        }
    }
}