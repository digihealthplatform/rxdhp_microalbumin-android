package com.dhp.albumin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TablePatient;
import com.dhp.albumin.ui.patient.ActivityPatientDetails;

public class AdapterPatientsList extends RecyclerView.Adapter<AdapterPatientsList.ViewHolder> {

    private List<TablePatient> tablePatients;

    private final TextView tvTvEmpty;

    private final Context context;
    private final LayoutInflater layoutInflater;
    private final DataManager dataManager;

    public AdapterPatientsList(Context context, DataManager dataManager, TextView tvTvEmpty) {
        this.context = context;
        this.dataManager = dataManager;
        this.tablePatients = dataManager.getAllPatients();
        this.layoutInflater = LayoutInflater.from(context);
        this.tvTvEmpty = tvTvEmpty;
    }

    @Override
    public AdapterPatientsList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_patient, parent, false);
        return new AdapterPatientsList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterPatientsList.ViewHolder holder, int position) {
        TablePatient tablePatient = tablePatients.get(position);
        holder.tvName.setText(tablePatient.getFullName());
        holder.tvPhone.setText("Phone: " + tablePatient.getPhone());
    }

    @Override
    public int getItemCount() {
        if (null != tablePatients) {
            return tablePatients.size();
        }

        return 0;
    }

    public void onSearchTextChanged(String searchText) {
        tablePatients = dataManager.getAllPatients(searchText);

        if (0 == tablePatients.size()) {
            tvTvEmpty.setVisibility(View.VISIBLE);
        } else {
            tvTvEmpty.setVisibility(View.GONE);
        }

        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName;
        private TextView tvPhone;

        ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_patient_name);
            tvPhone = (TextView) itemView.findViewById(R.id.tv_patient_phone);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityPatientDetails.class);
            intent.putExtra("patient_id", tablePatients.get(getAdapterPosition()).getPatientId());
            context.startActivity(intent);
        }
    }
}