package com.dhp.albumin.ui.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivityTest;

public class ActivityTestStep3 extends BaseAppCompatActivityTest {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_step_3);
        initToolbar(toolbar, getString(R.string.title_albumin_test), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);
    }

    @OnClick(R.id.btn_next)
    void next() {
        Intent intent = new Intent(this, ActivityTestStep4.class);
        startActivity(intent);
        finish();
    }
}