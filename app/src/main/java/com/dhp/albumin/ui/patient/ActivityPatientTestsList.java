package com.dhp.albumin.ui.patient;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.dhp.albumin.R;
import com.dhp.albumin.adapter.AdapterPatientsList;
import com.dhp.albumin.adapter.AdapterTestsList;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;

public class ActivityPatientTestsList extends BaseAppCompatActivity {

    private int patientId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_empty)
    TextView tvTvEmpty;
    @BindView(R.id.rv_tests_list)
    RecyclerView rvTestsList;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tests_list);
        initToolbar(toolbar, getString(R.string.title_tests), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        if (savedInstanceState != null) {
            patientId = savedInstanceState.getInt("patient_id");
        } else {
            patientId = getIntent().getIntExtra("patient_id", 0);
        }

        AdapterTestsList adapterTestsList = new AdapterTestsList(this, dataManager, patientId);
        rvTestsList.setLayoutManager(new LinearLayoutManager(this));
        rvTestsList.setAdapter(adapterTestsList);

        if (0 == dataManager.getAllTests(patientId).size()) {
            rvTestsList.setVisibility(View.GONE);
            tvTvEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("patient_id", patientId);
        super.onSaveInstanceState(outState);
    }
}