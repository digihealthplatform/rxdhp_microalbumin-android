package com.dhp.albumin.ui.test.renalyx;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.dhp.albumin.R;
import com.dhp.albumin.ui.test.ActivityTestReport;

public class SelectionNew extends Activity {

    String   name;
    Bitmap[] captured_images;
    String[] image_color_codes;
    String[] values;
    static String[] final_result;
    String[] colors;
    static String[] parameter_names;
    List<Map<String, String>> mapTable = new ArrayList<Map<String, String>>();

    List<List<Float>> mapKeys = new ArrayList<List<Float>>();

    List<Float> singleParamMap = new ArrayList<Float>();

    JsonParserNew jp = new JsonParserNew();

    float empty_lab[]      = new float[3];
    float empty_ref_lab[]  = new float[3];
    float yellow_ref_lab[] = new float[3];

    float closestKey[];

    float calculatedDistance[];

    float resultValue[];

    int totalParameters;

    String allprintdata[] = new String[2];

//    String referenceColor , albuminColor , pHcolor;

    List<float[]> capturedLabList = new ArrayList<float[]>();

    List<float[]> refLabList = new ArrayList<float[]>();

    float[] refLab = new float[3];

    float[] yellowcapturedLab = new float[3];


    TextView albLab, albRgb, phLab, phRgb, rfLab, rfRgb, albmapK, phmapK, alResultK, phResultK, albDelta, phDelta;

    TextView  resulttext;
    ImageView resultimage;

    SharedPreferences sharedPrefs;

    String yellowPatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_new);

        sharedPrefs = this.getSharedPreferences("com.example.youtestlatest", this.MODE_PRIVATE);

        yellowPatch = sharedPrefs.getString("yellowPatch", "");

        albLab = (TextView) findViewById(R.id.albLab);
        albRgb = (TextView) findViewById(R.id.albRgb);
        rfLab = (TextView) findViewById(R.id.refLab);
        rfRgb = (TextView) findViewById(R.id.refRgb);
        phLab = (TextView) findViewById(R.id.phLab);
        phRgb = (TextView) findViewById(R.id.phRgb);
        albmapK = (TextView) findViewById(R.id.albmapK);

        phmapK = (TextView) findViewById(R.id.phmapK);
        alResultK = (TextView) findViewById(R.id.alResultK);
        phResultK = (TextView) findViewById(R.id.phResultK);
        albDelta = (TextView) findViewById(R.id.albDelta);
        phDelta = (TextView) findViewById(R.id.phDelta);

        resultimage = (ImageView) findViewById(R.id.resultimage);

        resulttext = (TextView) findViewById(R.id.resulttext);

        //  getYellowPatchL();
        name = getmanufacturername();
        getparameternames();
        getcaptured_images();
        getimagecodes();
        getvalues();
        getcolors();
        gettablemap();
        colorDiffCalc();
        colorComparison();
    }

    public void getYellowPatchL() {
        int y_color = (int) Long.parseLong(yellowPatch, 16);

        int yrs = (y_color >> 16) & 0xFF;
        int ygs = (y_color >> 8) & 0xFF;
        int ybs = (y_color >> 0) & 0xFF;

        rgb2lab(yrs, ygs, ybs, yellow_ref_lab);
    }

    public void colorDiffCalc() {
        capturedLabList.clear();
        refLabList.clear();

        for (int i = 0; i < 2; i++) {
            int r_color = (int) Long.parseLong(InitialParser.initial_colors[i].substring(1, 7), 16);

            int rrs = (r_color >> 16) & 0xFF;
            int rgs = (r_color >> 8) & 0xFF;
            int rbs = (r_color >> 0) & 0xFF;

            System.out.println("i rcount : " + i);
            System.out.println("rrs : " + rrs);
            System.out.println("rgs : " + rgs);
            System.out.println("rbs : " + rbs);

            /*switch (i){
                case 0 :  albRgb.setText(String.valueOf("Rgb - "+rs+":"+gs+":"+bs));
                    break;
                case 1 :  rfRgb.setText(String.valueOf("Rgb - "+rs+":"+gs+":"+bs));
                    break;
                case 2 :  phRgb.setText(String.valueOf("Rgb - "+rs+":"+gs+":"+bs));
                    break;
            }
*/

            //   capturedLabList.add(i, rgb2lab(rs, gs, bs, empty_lab));

            refLabList.add(i, rgb2lab(rrs, rgs, rbs, empty_ref_lab));

            empty_ref_lab = new float[3];
        }

        for (int i = 0; i < image_color_codes.length; i++) {

            int s_color = (int) Long.parseLong(image_color_codes[i].substring(1, 7), 16);

            int rs = (s_color >> 16) & 0xFF;
            int gs = (s_color >> 8) & 0xFF;
            int bs = (s_color >> 0) & 0xFF;

            System.out.println("i count : " + i);
            System.out.println("rs : " + rs);
            System.out.println("gs : " + gs);
            System.out.println("bs : " + bs);

            switch (i) {
                case 0:
                    albRgb.setText(String.valueOf("Rgb - " + rs + ":" + gs + ":" + bs));
                    break;
                case 1:
                    rfRgb.setText(String.valueOf("Rgb - " + rs + ":" + gs + ":" + bs));
                    break;
                case 2:
                    phRgb.setText(String.valueOf("Rgb - " + rs + ":" + gs + ":" + bs));
                    break;
            }

            capturedLabList.add(i, rgb2lab(rs, gs, bs, empty_lab));

            empty_lab = new float[3];
        }

        albLab.setText(String.valueOf("Lab - " + capturedLabList.get(0)[0] + ":" + capturedLabList.get(0)[1] + ":" + capturedLabList.get(0)[2]));
        rfLab.setText(String.valueOf("Lab - " + refLabList.get(0)[0] + ":" + refLabList.get(0)[1] + ":" + refLabList.get(0)[2]));

       /* Resultclass.albumin = Integer.parseInt(final_value);
        Resultclass.colour = suggested_image_codes[scolorpos].substring(1,7) ;
     */

        // phLab.setText(String.valueOf("Lab - "+capturedLabList.get(2)[0] + ":" + capturedLabList.get(2)[1] + ":" + capturedLabList.get(2)[2]));


        yellowcapturedLab = capturedLabList.get(1);

        refLab = refLabList.get(0);

        capturedLabList.remove(1);

        calculatedDistance = new float[totalParameters];

        closestKey = new float[totalParameters];

        System.out.println("Manav Ref LAB : " + refLab[0] + ":" + refLab[1] + ":" + refLab[2]);

        Resultclass.refLab = String.valueOf(refLab[0]);

        float labchange = 0f;

        for (int i = 0; i < totalParameters; i++) {
            System.out.println("Manav captured LAB " + String.valueOf(i) + " : " + capturedLabList.get(i)[0] + ":" + capturedLabList.get(i)[1] + ":" + capturedLabList.get(i)[2]);
            Resultclass.capLab = String.valueOf(capturedLabList.get(i)[0]);
            calculatedDistance[i] = refLab[0] - capturedLabList.get(i)[0];
            ;//cide2000colordistance(capturedLabList.get(i), refLab);

            labchange = cide2000colordistance(capturedLabList.get(i), refLab);

            System.out.println("calculated Distances : " + calculatedDistance[i]);
        }

        albDelta.setText(String.valueOf(calculatedDistance[0]));
        phDelta.setText(String.valueOf(labchange));


    }

    public void colorComparison() {


        for (int i = 0; i < mapTable.size(); i++) {

            allprintdata[i] = "map :";
            for (Map.Entry<String, String> entry : mapTable.get(i).entrySet()) {
                System.out.println(i + " Key = " + entry.getKey() + ", Value = " + entry.getValue());
                allprintdata[i] = allprintdata[i].concat(String.valueOf("{" + entry.getKey() + "," + entry.getValue() + "} "));
                singleParamMap.add(Float.valueOf(entry.getKey()));

            }

            mapKeys.add(singleParamMap);

            singleParamMap = new ArrayList<Float>();

        }


        albmapK.setText(allprintdata[0]);
        //  phmapK.setText(allprintdata[1]);


        for (int i = 0; i < calculatedDistance.length; i++) {


            closestKey[i] = Math.round(closest(calculatedDistance[i], mapKeys.get(i)));

            System.out.println(" closest key : Al : " + closestKey[0]);//+" , PH : "+closestKey[1]);


        }

        findresultValue(calculatedDistance[0]);

    }


    public float[] rgb2lab(int R, int G, int B, float[] lab) {
        // http://www.brucelindbloom.com

        float r, g, b, X, Y, Z, fx, fy, fz, xr, yr, zr;
        float Ls, as, bs;
        float eps = 216.f / 24389.f;
        float k   = 24389.f / 27.f;

        float Xr = 0.964221f; // reference white D50
        float Yr = 1.0f;
        float Zr = 0.825211f;

        // RGB to XYZ
        r = R / 255.f; // R 0..1
        g = G / 255.f; // G 0..1
        b = B / 255.f; // B 0..1

        // assuming sRGB (D65)
        if (r <= 0.04045)
            r = r / 12;
        else
            r = (float) Math.pow((r + 0.055) / 1.055, 2.4);

        if (g <= 0.04045)
            g = g / 12;
        else
            g = (float) Math.pow((g + 0.055) / 1.055, 2.4);

        if (b <= 0.04045)
            b = b / 12;
        else
            b = (float) Math.pow((b + 0.055) / 1.055, 2.4);

        X = 0.436052025f * r + 0.385081593f * g + 0.143087414f * b;
        Y = 0.222491598f * r + 0.71688606f * g + 0.060621486f * b;
        Z = 0.013929122f * r + 0.097097002f * g + 0.71418547f * b;

        // XYZ to Lab
        xr = X / Xr;
        yr = Y / Yr;
        zr = Z / Zr;

        if (xr > eps)
            fx = (float) Math.pow(xr, 1 / 3.);
        else
            fx = (float) ((k * xr + 16.) / 116.);

        if (yr > eps)
            fy = (float) Math.pow(yr, 1 / 3.);
        else
            fy = (float) ((k * yr + 16.) / 116.);

        if (zr > eps)
            fz = (float) Math.pow(zr, 1 / 3.);
        else
            fz = (float) ((k * zr + 16.) / 116);

        Ls = (116 * fy) - 16;
        as = 500 * (fx - fy);
        bs = 200 * (fy - fz);


        lab[0] = (float) Ls;
        lab[1] = (float) as;
        lab[2] = (float) bs;

        System.out.println("Manav Captured LAB : " + lab[0] + ":" + lab[1] + ":" + lab[2]);

        return lab;
    }

    private float cide2000colordistance(final float[] lab1, final float[] lab2) {

        /*Added a hue term for better percuptual uniformity
        Compensation for neutral colors
        Compensation for lightness
        Compensation for chroma
        Compensation for hue*/
        //Set weighting factors to 1
        double k_L = 1.0d;
        double k_C = 1.0d;
        double k_H = 1.0d;

        double LAB1_L = (double) lab1[0];
        double LAB1_A = (double) lab1[1];
        double LAB1_B = (double) lab1[2];
        double LAB2_L = (double) lab2[0];
        double LAB2_A = (double) lab2[1];
        double LAB2_B = (double) lab2[2];

        //Calculate Cprime1, Cprime2, Cabbar
        double c_star_1_ab       = Math.sqrt(LAB1_A * LAB1_A + LAB1_B * LAB1_B);
        double c_star_2_ab       = Math.sqrt(LAB2_A * LAB2_A + LAB2_B * LAB2_B);
        double c_star_average_ab = (c_star_1_ab + c_star_2_ab) / 2;

        double c_star_average_ab_pot7 = c_star_average_ab * c_star_average_ab * c_star_average_ab;
        c_star_average_ab_pot7 *= c_star_average_ab_pot7 * c_star_average_ab;

        double G        = 0.5d * (1 - Math.sqrt(c_star_average_ab_pot7 / (c_star_average_ab_pot7 + 6103515625.0))); //25^7
        double a1_prime = (1 + G) * LAB1_A;
        double a2_prime = (1 + G) * LAB2_A;

        double C_prime_1 = Math.sqrt(a1_prime * a1_prime + LAB1_B * LAB1_B);
        double C_prime_2 = Math.sqrt(a2_prime * a2_prime + LAB2_B * LAB2_B);
        //Angles in Degree.
        double h_prime_1 = ((Math.atan2(LAB1_B, a1_prime) * 180d / Math.PI) + 360) % 360d;
        double h_prime_2 = ((Math.atan2(LAB2_B, a2_prime) * 180d / Math.PI) + 360) % 360d;

        double delta_L_prime = LAB2_L - LAB1_L;
        double delta_C_prime = C_prime_2 - C_prime_1;

        double h_bar = Math.abs(h_prime_1 - h_prime_2);
        double delta_h_prime;
        if (C_prime_1 * C_prime_2 == 0) delta_h_prime = 0;
        else {
            if (h_bar <= 180d) {
                delta_h_prime = h_prime_2 - h_prime_1;
            } else if (h_bar > 180d && h_prime_2 <= h_prime_1) {
                delta_h_prime = h_prime_2 - h_prime_1 + 360.0;
            } else {
                delta_h_prime = h_prime_2 - h_prime_1 - 360.0;
            }
        }
        double delta_H_prime = 2 * Math.sqrt(C_prime_1 * C_prime_2) * Math.sin(delta_h_prime * Math.PI / 360d);

        // Calculate CIEDE2000
        double L_prime_average = (LAB1_L + LAB2_L) / 2d;
        double C_prime_average = (C_prime_1 + C_prime_2) / 2d;

        //Calculate h_prime_average

        double h_prime_average;
        if (C_prime_1 * C_prime_2 == 0) h_prime_average = 0;
        else {
            if (h_bar <= 180d) {
                h_prime_average = (h_prime_1 + h_prime_2) / 2;
            } else if (h_bar > 180d && (h_prime_1 + h_prime_2) < 360d) {
                h_prime_average = (h_prime_1 + h_prime_2 + 360d) / 2;
            } else {
                h_prime_average = (h_prime_1 + h_prime_2 - 360d) / 2;
            }
        }
        double L_prime_average_minus_50_square = (L_prime_average - 50);
        L_prime_average_minus_50_square *= L_prime_average_minus_50_square;

        double S_L = 1 + ((.015d * L_prime_average_minus_50_square) / Math.sqrt(20 + L_prime_average_minus_50_square));
        double S_C = 1 + .045d * C_prime_average;
        double T = 1
                - .17 * Math.cos(DegToRad(h_prime_average - 30))
                + .24 * Math.cos(DegToRad(h_prime_average * 2))
                + .32 * Math.cos(DegToRad(h_prime_average * 3 + 6))
                - .2 * Math.cos(DegToRad(h_prime_average * 4 - 63));
        double S_H                                     = 1 + .015 * T * C_prime_average;
        double h_prime_average_minus_275_div_25_square = (h_prime_average - 275) / (25);
        h_prime_average_minus_275_div_25_square *= h_prime_average_minus_275_div_25_square;
        double delta_theta = 30 * Math.exp(-h_prime_average_minus_275_div_25_square);

        double C_prime_average_pot_7 = C_prime_average * C_prime_average * C_prime_average;
        C_prime_average_pot_7 *= C_prime_average_pot_7 * C_prime_average;
        double R_C = 2 * Math.sqrt(C_prime_average_pot_7 / (C_prime_average_pot_7 + 6103515625.0));

        double R_T = -Math.sin(DegToRad(2 * delta_theta)) * R_C;

        double delta_L_prime_div_k_L_S_L = delta_L_prime / (S_L * k_L);
        double delta_C_prime_div_k_C_S_C = delta_C_prime / (S_C * k_C);
        double delta_H_prime_div_k_H_S_H = delta_H_prime / (S_H * k_H);

        double CIEDE2000 = Math.sqrt(
                delta_L_prime_div_k_L_S_L * delta_L_prime_div_k_L_S_L
                        + delta_C_prime_div_k_C_S_C * delta_C_prime_div_k_C_S_C
                        + delta_H_prime_div_k_H_S_H * delta_H_prime_div_k_H_S_H
                        + R_T * delta_C_prime_div_k_C_S_C * delta_H_prime_div_k_H_S_H
        );

        return (float) CIEDE2000;
    }


    private double DegToRad(double degrees) {
        return degrees * Math.PI / 180;
    }


    public void findresultValue(float calc_key) {

        float[]  compare_colors = new float[6];
        String[] colors_value   = new String[6];

        int x = 0;

        resultValue = new float[totalParameters];


//       mapTable.


        for (int i = 0; i < mapTable.size(); i++) {

            for (Map.Entry<String, String> entry : mapTable.get(i).entrySet()) {
                System.out.println(i + " Key = " + entry.getKey() + ", Value = " + entry.getValue());
                System.out.println("Value = " + closestKey[i]);

                compare_colors[x] = Float.valueOf(entry.getKey());
                colors_value[x] = entry.getValue();


                if (entry.getKey().equals(String.valueOf(Math.round(closestKey[i])))) {

                    System.out.println("condition true");

                    resultValue[i] = Float.valueOf(entry.getValue());
                }

                x = x + 1;

            }


        }

        alResultK.setText(String.valueOf(resultValue[0]));
        //  phResultK.setText(String.valueOf(resultValue[1]));

        System.out.println(" result values : Al : " + resultValue[0]);//+" , PH : "+resultValue[1]);

        trylinearsearch(compare_colors, colors_value, calc_key);


    }

    public float closest(float of, List<Float> in) {
        float min     = Float.MAX_VALUE;
        float closest = of;

        for (float v : in) {
            final float diff = Math.abs(v - of);

            if (diff < min) {
                min = diff;
                closest = v;
            }
        }

        return closest;
    }

    public void trylinearsearch(float[] comparing_colors2, String[] values, float sl) {

        System.out.println("in linearsearch : " + sl);

        float captured_l = sl;

        System.out.println("yellow captured : " + yellowcapturedLab[0] + " , yellow ref :" + yellowPatch);

        /*if(yellowcapturedLab[0] > Float.parseFloat(yellowPatch)){

            captured_l = sl - (yellowcapturedLab[0] - Float.parseFloat(yellowPatch));



        }*/

        float  map_l[] = new float[8];
        String l_val[] = new String[8];
        float  output;
        int    outputint;

        // for(int i = 0 ; i < comparing_colors2.length ; i++ ){

        map_l[0] = 0f;
        map_l[1] = comparing_colors2[0];
        map_l[2] = comparing_colors2[1];
        map_l[3] = comparing_colors2[2];
        map_l[4] = comparing_colors2[3];
        map_l[5] = comparing_colors2[4];
        map_l[6] = comparing_colors2[5];
        map_l[7] = 100f;

        l_val[0] = "0";
        l_val[1] = values[0];
        l_val[2] = values[1];
        l_val[3] = values[2];
        l_val[4] = values[3];
        l_val[5] = values[4];
        l_val[6] = values[5];
        l_val[7] = "1000";

        Map<Float, String> mapData = new HashMap<Float, String>();
        //}

        for (int i = 0; i < 8; i++) {

            System.out.print("map l list : " + map_l[i] + " - ");
            System.out.println("value list : " + l_val[i]);
            mapData.put(map_l[i], l_val[i]);

        }


        float prev = 0;
        float next = 0;
        for (int k = 0; k < map_l.length; k++) {


            if (k == 0) {

                prev = map_l[k];
            }

            if (captured_l == map_l[k]) {
                output = Float.valueOf(l_val[k]);
                System.out.println("matched : " + output);

            } else {
                float cur = map_l[k];
                if (captured_l > prev && captured_l < cur) {

                    System.out.println("prev : " + prev + "next : " + cur);
                    System.out.println(captured_l);
                    next = cur;
                    break;
                } else {
                    prev = cur;
                }

            }
        }

        float vmplus1 = Float.valueOf(mapData.get(next));
        float vm      = Float.valueOf(mapData.get(prev));
        float kmplus1 = next;
        float km      = prev;

        float slope = (vmplus1 - vm) / (kmplus1 - km);

        float deltavn = slope * (captured_l - km);

        output = vm + deltavn;

        System.out.println("final linear op :" + output);
        // graphdata.setText(String.valueOf(op));
        phResultK.setText(String.valueOf(output));

        if (output <= 40) {
            resultimage.setImageResource(R.drawable.kidneygreen);
            resulttext.setText("Below 30");
            Resultclass.result = "Below 30";
        } else if ((output >= 41) && output <= 160) {
            resultimage.setImageResource(R.drawable.kidneyyellow);
            resulttext.setText("Between 30 - 150");
            Resultclass.result = "Between 30 - 150";
        } else if ((output >= 161) && output <= 310) {
            resultimage.setImageResource(R.drawable.orange);

            resulttext.setText("Between 150-300");
            Resultclass.result = "Between 150-300";
        } else if (output >= 311) {

            resultimage.setImageResource(R.drawable.kidneyred);

            resulttext.setText("Above 300");
            Resultclass.result = "Above 300";
        }

//        Resultclass.albuminvalue = String.valueOf(output);

        outputint = (int) output;
        Resultclass.albuminvalue = String.valueOf(outputint);

        Intent i = new Intent(SelectionNew.this, ActivityTestReport.class);
        startActivity(i);
        finish();
        // Resultclass.lab = String.valueOf(op); //not lab but graph o/p
    }

    private void getcolors() {
        colors = jp.color;
    }

    private void getparameternames() {
        // TODO Auto-generated method stub
        parameter_names = jp.pname;
        totalParameters = jp.count;
        System.out.println("totalParameters length : " + totalParameters);
    }

    private void getvalues() {
        // TODO Auto-generated method stub
        values = jp.value;
    }

    private void getcaptured_images() {
        captured_images = jp.captured_images;
    }

    private String getmanufacturername() {
        return jp.manufacturer_name;
        // TODO Auto-generated method stub
    }

    private void getimagecodes() {
        image_color_codes = jp.images_color_code;

        //   albuminColor = image_color_codes[0];
        //   referenceColor = image_color_codes[1];
        //   pHcolor = image_color_codes[2];
    }

    private void gettablemap() {
        mapTable = jp.mapTable;
        Map<String, String> treeMap = new TreeMap<String, String>(mapTable.get(0));

        //mapData.clear();
        TreeMap<String, String> sortedMap = new TreeMap<String, String>();

        Set      set2      = treeMap.entrySet();
        Iterator iterator2 = set2.iterator();
        while (iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry) iterator2.next();
            System.out.print("manav sort");
            System.out.print(me2.getKey() + ": ");
            System.out.println(me2.getValue());
            sortedMap.put(me2.getKey().toString(), me2.getValue().toString());
        }

        mapTable.clear();
        mapTable.add(sortedMap);

        for (int i = 0; i < mapTable.size(); i++) {

            for (Map.Entry<String, String> entry : mapTable.get(i).entrySet()) {
                System.out.println(i + "Sorted Key = " + entry.getKey() + ",Sorted Value = " + entry.getValue());
                //System.out.println("Sorted Value = " + closestKey[i]);
            }
        }
    }
}

/*
class LoadAllProducts extends AsyncTask<String, String, String> {

    */
/**
 * Before starting background thread Show Progress Dialog
 * <p>
 * getting All products from url
 * <p>
 * After completing background task Dismiss the progress dialog
 * <p>
 * Updating parsed JSON data into ListView
 * <p>
 * Before starting background thread Show Progress Dialog
 * <p>
 * Creating product
 * <p>
 * After completing background task Dismiss the progress dialog
 * <p>
 * getting All products from url
 * <p>
 * After completing background task Dismiss the progress dialog
 * <p>
 * Updating parsed JSON data into ListView
 * <p>
 * Before starting background thread Show Progress Dialog
 * <p>
 * Creating product
 * <p>
 * After completing background task Dismiss the progress dialog
 **//*

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    */
/**
 * getting All products from url
 * *//*

    protected String doInBackground(String... args) {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        // getting JSON string from URL
        JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);

        // Check your log cat for JSON reponse
        Log.d("All Products: ", json.toString());

        try {
            // Checking for SUCCESS TAG
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {
                // products found
                // Getting Array of Products
                products = json.getJSONArray(TAG_PRODUCTS);

                // looping through All Products
                for (int i = 0; i < products.length(); i++) {
                    JSONObject c = products.getJSONObject(i);

                    // Storing each json item in variable
                    String id = c.getString(TAG_PID);
                    String name = c.getString(TAG_NAME);

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put(TAG_PID, id);
                    map.put(TAG_NAME, name);

                    // adding HashList to ArrayList
                    productsList.add(map);
                }
            } else {
                // no products found
                // Launch Add New product Activity
                Intent i = new Intent(getApplicationContext(),
                        NewProductActivity.class);
                // Closing all previous activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    */
/**
 * After completing background task Dismiss the progress dialog
 * **//*

    protected void onPostExecute(String file_url) {
        // dismiss the dialog after getting all products

        // updating UI from Background Thread
        runOnUiThread(new Runnable() {
            public void run() {
                */
/**
 * Updating parsed JSON data into ListView
 * *//*


            }
        });

    }

}

*/

// to insert data



/*

class CreateNewProduct extends AsyncTask<String, String, String> {

    */
/**
 * Before starting background thread Show Progress Dialog
 * *//*

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    */
/**
 * Creating product
 * *//*

    protected String doInBackground(String... args) {
        String name = inputName.getText().toString();
        String price = inputPrice.getText().toString();
        String description = inputDesc.getText().toString();


        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("price", price));
        params.add(new BasicNameValuePair("description", description));

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(url_create_product,
                "POST", params);

        // check log cat fro response
        Log.d("Create Response", json.toString());

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {
                // successfully created product
                Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
                startActivity(i);

                // closing this screen
                finish();
            } else {
                // failed to create product
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    */
/**
 * After completing background task Dismiss the progress dialog
 * **//*

    protected void onPostExecute(String file_url) {
        // dismiss the dialog once done

    }

}*/
