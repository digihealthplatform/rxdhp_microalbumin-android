package com.dhp.albumin.ui.test.renalyx;

public class Resultclass {

    public static int    bp_sistotic;
    public static int    bp_distotic;
    public static int    diabitese;
    public static float  albumin;
    public static float  albumin_correction;
    public static String colour;
    public static String name;
    public static String gender;
    public static int    age;
    public static String result;
    public static String location;
    public static String id;
    public static String refLab;
    public static String capLab;
    public static String albuminvalue;

    public static void reset() {
        bp_sistotic = 0;
        bp_distotic = 0;
        diabitese = 0;
        albumin = 0;
        colour = " ";
        name = " ";
        gender = " ";
        age = 0;
        result = " ";
        location = " ";
        id = " ";
        refLab = " ";
        capLab = " ";
        albuminvalue = " ";
    }

    public static String getLocation() {
        return location;
    }

    public static void setLocation(String location) {
        Resultclass.location = location;
    }

    public static String getColour() {
        return colour;
    }

    public static void setColour(String colour) {
        Resultclass.colour = colour;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Resultclass.name = name;
    }

    public static String getGender() {
        return gender;
    }

    public static void setGender(String gender) {
        Resultclass.gender = gender;
    }

    public static int getAge() {
        return age;
    }

    public static void setAge(int age) {
        Resultclass.age = age;
    }

    public static String getResult() {
        return result;
    }

    public static void setResult(String result) {
        Resultclass.result = result;
    }

    public static String getAlbuminvalue() {
        return albuminvalue;
    }

    public static void setAlbuminvalue(String albuminvalue) {
        Resultclass.albuminvalue = albuminvalue;
    }

    public int getBp_sistotic() {
        return bp_sistotic;
    }

    public int getBp_distotic() {
        return bp_distotic;
    }

    public int getDiabitese() {
        return diabitese;
    }

    public float getAlbumin() {
        return albumin;
    }
}
