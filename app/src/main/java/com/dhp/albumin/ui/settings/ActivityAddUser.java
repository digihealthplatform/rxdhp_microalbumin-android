package com.dhp.albumin.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TableUser;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.utils.LToast;

public class ActivityAddUser extends BaseAppCompatActivity {
    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_full_name)
    EditText etFullName;
    @BindView(R.id.et_user_id)
    EditText etUserId;
    @BindView(R.id.et_password)
    EditText etPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        initToolbar(toolbar, getString(R.string.title_add_new_user), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);
    }

    @OnClick(R.id.btn_add_user)
    void addUser() {

        TableUser tableUser = new TableUser();
        tableUser.setFullName(etFullName.getText().toString());
        tableUser.setUserName(etUserId.getText().toString());
        tableUser.setPassword(etPassword.getText().toString());


        if (tableUser.getUserName().equals("")) {
            LToast.warning(R.string.error_full_name);
            return;
        }

        if (tableUser.getUserName().equals("")) {
            LToast.warning(R.string.error_user_id);
            return;
        }

        if (tableUser.getPassword().equals("")) {
            LToast.warning(R.string.error_password);
            return;
        }

        if (dataManager.createUser(tableUser)) {
            LToast.success(R.string.new_user_added);
            finish();
        } else {
            LToast.warning(R.string.error_user_id_exists);
        }
    }
}