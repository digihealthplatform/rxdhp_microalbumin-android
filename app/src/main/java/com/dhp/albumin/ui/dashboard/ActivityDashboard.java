package com.dhp.albumin.ui.dashboard;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TablePatient;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.ui.help.ActivityHelp;
import com.dhp.albumin.ui.login.ActivityLogin;
import com.dhp.albumin.ui.patient.ActivityPatientsList;
import com.dhp.albumin.ui.settings.ActivitySettings;
import com.dhp.albumin.ui.statistics.ActivityStatistics;
import com.dhp.albumin.ui.test.ActivityCameraView;
import com.dhp.albumin.ui.test.ActivitySelectTest;
import com.dhp.albumin.utils.LEnums;
import com.dhp.albumin.utils.LToast;
import com.dhp.albumin.utils.LUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.albumin.utils.LConstants.TYPE_CALIBRATE;
import static com.dhp.albumin.utils.LUtils.calculateAge;
import static com.dhp.albumin.utils.LUtils.getFormattedDate;

public class ActivityDashboard extends BaseAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private LEnums.DashboardState dashboardState = LEnums.DashboardState.NOT_SAVED;

    private String       gender;
    private TablePatient tablePatient;

    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar      toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.et_age)
    EditText etAge;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_address)
    EditText etAddress;

    @BindView(R.id.iv_select_dob)
    ImageView ivSelectDob;
    @BindView(R.id.iv_select_address)
    ImageView ivSelectAddress;

    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    @BindView(R.id.btn_clear)
    Button btnClear;
    @BindView(R.id.btn_save_and_test)
    Button btnSaveAndTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initToolbar(toolbar, getString(R.string.title_dashboard), false);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initUi();

        View     header     = navigationView.getHeaderView(0);
        TextView tvUserName = (TextView) header.findViewById(R.id.tv_user_name);
        tvUserName.setText(dataManager.getUser(dataManager.getLoggedInUserId()).getFullName());
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUi() {
        etDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    selectDob();
                }
                return false;
            }
        });

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                gender = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            confirmExit();
        }
    }

    private void confirmExit() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.message_confirm_exit));
        builder.setPositiveButton(getString(R.string.btn_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityDashboard.super.onBackPressed();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_patients:
                startActivity(new Intent(this, ActivityPatientsList.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case R.id.nav_calibrate:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(ActivityDashboard.this, ActivityCameraView.class);
                        intent.putExtra("activity_type", TYPE_CALIBRATE);
                        startActivity(intent);
                    }
                }, 220);

                break;

            case R.id.nav_settings:
                startActivityDelayed(ActivitySettings.class);
                break;

            case R.id.nav_statistics:
                startActivityDelayed(ActivityStatistics.class);
                break;

            case R.id.nav_help:
                startActivityDelayed(ActivityHelp.class);
                break;

            case R.id.nav_logout:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        logout();
                    }
                }, 150);
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void startActivityDelayed(final Class className) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(ActivityDashboard.this, className));
            }
        }, 220);
    }

    private void logout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.message_confirm_logout));
        builder.setPositiveButton(getString(R.string.btn_logout), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.setLoggedIn(false, "");
                finish();
                startActivity(new Intent(ActivityDashboard.this, ActivityLogin.class));
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    @OnClick(R.id.iv_select_dob)
    void selectDob() {

        if (LEnums.DashboardState.SAVED == dashboardState) {
            return;
        }

        final Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        onDobSet(dayOfMonth, monthOfYear, year);
                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "DatePickerDialogDob");
    }

    private void onDobSet(int dayOfMonth, int monthOfYear, int year) {
        if (LUtils.isFutureDate(dayOfMonth, monthOfYear, year)) {
            LToast.warning(R.string.error_fututre_date);
            return;
        }

        etDob.setText(getFormattedDate(dayOfMonth, monthOfYear, year));
        etAge.setText("" + calculateAge(dayOfMonth, monthOfYear, year));
        etAge.setEnabled(false);
    }

    @OnClick(R.id.btn_clear)
    void clear() {

        if (LEnums.DashboardState.NOT_SAVED == dashboardState) {
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.message_confirm_clear));
            builder.setPositiveButton(getString(R.string.btn_clear), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    tablePatient = null;
                    dashboardState = LEnums.DashboardState.NOT_SAVED;
                    btnSaveAndTest.setText(getString(R.string.save));
                    etName.requestFocus();
                    clearFields();
                    setFieldsEnabled(true);
                }
            });
            builder.setNegativeButton(getString(R.string.btn_cancel), null);
            builder.show();

        } else {
            tablePatient = null;
            dashboardState = LEnums.DashboardState.NOT_SAVED;
            btnSaveAndTest.setText(getString(R.string.save));
            etName.requestFocus();
            clearFields();
            setFieldsEnabled(true);
        }
    }

    @OnClick(R.id.btn_save_and_test)
    void saveAndTest() {
        if (LEnums.DashboardState.NOT_SAVED == dashboardState) {
            readValues();

            if (ifAllFieldsFilled()) {
                if (dataManager.createPatient(tablePatient)) {
                    setFieldsEnabled(false);
                    dashboardState = LEnums.DashboardState.SAVED;
                    btnSaveAndTest.setText(getString(R.string.start_test));
                    LToast.success(R.string.patient_created);
                } else {
                    LToast.warning(R.string.error_phone_exists);
                }
            }
        } else {
            Intent intent = new Intent(this, ActivitySelectTest.class);
            intent.putExtra("patient_id", dataManager.getPatientId(tablePatient.getPhone()));
            startActivity(intent);
        }
    }

    private boolean ifAllFieldsFilled() {
        if (tablePatient.getFullName().equals("")) {
            LToast.warning(R.string.error_name);
            return false;
        }

        if (tablePatient.getDob().equals("") && tablePatient.getAge() <= 0) {
            LToast.warning(R.string.error_dob);
            return false;
        }

        if (null == tablePatient.getGender()) {
            LToast.warning(R.string.error_gender);
            return false;
        }

        if (!Patterns.PHONE.matcher(tablePatient.getPhone()).matches()) {
            LToast.warning(R.string.error_phone);
            return false;
        }

        if (tablePatient.getAddress().equals("")) {
            LToast.warning(R.string.error_address);
            return false;
        }

        return true;
    }

    private void readValues() {
        tablePatient = new TablePatient();
        tablePatient.setFullName(etName.getText().toString());
        tablePatient.setDob(etDob.getText().toString());

        try {
            tablePatient.setAge(Integer.parseInt(etAge.getText().toString()));
        } catch (Exception e) {
        }

        tablePatient.setGender(gender);
        tablePatient.setPhone(etPhone.getText().toString());
        tablePatient.setAddress(etAddress.getText().toString());
    }

    private void clearFields() {
        etName.setText("");
        etDob.setText("");
        etAge.setText("");
        rgGender.clearCheck();
        etPhone.setText("");
        etAddress.setText("");
    }

    private void setFieldsEnabled(boolean enabled) {
        etName.setEnabled(enabled);
        etDob.setEnabled(enabled);
        ivSelectDob.setEnabled(enabled);
        etAge.setEnabled(enabled);

        for (int i = 0; i < rgGender.getChildCount(); i++) {
            rgGender.getChildAt(i).setEnabled(enabled);
        }

        etPhone.setEnabled(enabled);
        etAddress.setEnabled(enabled);

        if (enabled) {
            ivSelectDob.setAlpha(1);
            ivSelectAddress.setAlpha(1);
        } else {
            ivSelectDob.setAlpha(0.5f);
            ivSelectAddress.setAlpha(0.5f);
        }
    }
}
