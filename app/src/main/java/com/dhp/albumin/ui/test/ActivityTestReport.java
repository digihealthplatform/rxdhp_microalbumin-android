package com.dhp.albumin.ui.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TablePatient;
import com.dhp.albumin.data.table.TableTest;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.ui.dashboard.ActivityDashboard;
import com.dhp.albumin.ui.test.renalyx.CustomsLogs;
import com.dhp.albumin.ui.test.renalyx.Resultclass;
import com.dhp.albumin.utils.LUtils;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.albumin.utils.LUtils.getResultColor;

public class ActivityTestReport extends BaseAppCompatActivity {

    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_albumin_value)
    TextView tvAlbuminValue;

    @BindView(R.id.tv_diagnosis)
    TextView tvDiagnosis;

    @BindView(R.id.tv_albumin_value_with_correction)
    TextView tvAlbuminValueCorrection;

    @BindView(R.id.tv_diagnosis_with_correction)
    TextView tvDiagnosisCorrection;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_age)
    TextView tvAge;

    @BindView(R.id.tv_gender)
    TextView tvGender;

    @BindView(R.id.tv_phone)
    TextView tvPhone;

    @BindView(R.id.tv_test_date)
    TextView tvTestDate;

    @BindView(R.id.view_result_color)
    View viewResultColor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_report);
        initToolbar(toolbar, getString(R.string.title_test_report), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        String p_id                 = Resultclass.id;
        String p_name               = Resultclass.name;
        int    p_age                = Resultclass.age;
        String p_gender             = Resultclass.gender;
        String p_location           = Resultclass.location;
        String p_result             = Resultclass.result;
        int    p_systolic           = Resultclass.bp_sistotic;
        int    p_dystolic           = Resultclass.bp_distotic;
        int    p_glucose            = Resultclass.diabitese;
        float  p_albumin            = Resultclass.albumin;
        float  p_albumin_correction = Resultclass.albumin_correction;
        String p_color              = Resultclass.colour;
        String p_refl               = Resultclass.refLab;
        String p_capl               = Resultclass.capLab;
        String p_albuminvalue       = Resultclass.albuminvalue;

        TablePatient tablePatient = dataManager.getPatient(dataManager.getCurrentTestPatientId());

        Calendar today = Calendar.getInstance();

/*        if (p_albumin <= 10) {
            p_result = "Below 10";

        } else if ((p_albumin > 10) && (p_albumin <= 30)) {
            p_result = "Between 10 - 30";

        } else if ((p_albumin > 30) && (p_albumin <= 80)) {
            p_result = "Between 30 - 80";

        } else if ((p_albumin > 80) && (p_albumin <= 150)) {
            p_result = "Between 80 - 150";

        } else if ((p_albumin > 150) && (p_albumin <= 300)) {
            p_result = "Between 150 - 300";

        } else {
            p_result = "Above 300";
        }*/

        if (p_albumin <= 30) {
            p_result = "Below 30";

        } else if ((p_albumin > 30) && (p_albumin <= 150)) {
            p_result = "Between 30 - 150";

        } else {
            p_result = "Above 150";
        }

        String p_result_correction = "";

  /*      if (p_albumin_correction <= 10) {
            p_result_correction = "Below 10";

        } else if ((p_albumin_correction > 10) && (p_albumin_correction <= 30)) {
            p_result_correction = "Between 10 - 30";

        } else if ((p_albumin_correction > 30) && (p_albumin_correction <= 80)) {
            p_result_correction = "Between 30 - 80";

        } else if ((p_albumin_correction > 80) && (p_albumin_correction <= 150)) {
            p_result_correction = "Between 80 - 150";

        } else if ((p_albumin_correction > 150) && (p_albumin_correction <= 300)) {
            p_result_correction = "Between 150 - 300";

        } else {
            p_result_correction = "Above 300";
        }*/

        if (p_albumin_correction <= 30) {
            p_result_correction = "Below 30";

        } else if ((p_albumin_correction > 30) && (p_albumin_correction <= 150)) {
            p_result_correction = "Between 30 - 150";

        } else {
            p_result_correction = "Above 150";
        }

        TableTest tableTest = new TableTest();
        tableTest.setPatientId(tablePatient.getPatientId());
        tableTest.setUserId(dataManager.getLoggedInUserId());
        tableTest.setTestType(dataManager.getCurrentTestType());
        tableTest.setTestDateTime(today.getTimeInMillis());
        tableTest.setAlbuminValue("" + p_albumin);
        tableTest.setAlbuminValueCorrection("" + p_albumin_correction);
        tableTest.setDiagnosis(p_result);
        tableTest.setDiagnosisCorrection(p_result_correction);
        dataManager.createTest(tableTest);

        tvName.setText(tablePatient.getFullName());
        tvAge.setText("" + tablePatient.getAge());
        tvGender.setText(tablePatient.getGender());
        tvPhone.setText(tablePatient.getPhone());
        tvTestDate.setText(LUtils.getFormattedDateTime(today.getTimeInMillis()));
        tvDiagnosis.setText(tableTest.getDiagnosis());
        tvDiagnosisCorrection.setText(tableTest.getDiagnosisCorrection());
        viewResultColor.setBackgroundColor(getResultColor(p_albumin));

        if (dataManager.getBooleanPref("show_albumin", false)) {
            tvAlbuminValue.setText("Albumin Value: " + tableTest.getAlbuminValue());
            tvAlbuminValueCorrection.setText("Albumin Value: " + tableTest.getAlbuminValueCorrection());
        }

        Resultclass.id = "" + tablePatient.getPatientId();
        CustomsLogs.filecreation();

        // TODO: 23-Aug-18, Uncomment if write is needed
//        CustomsLogs.write_to_file(tvTestDate.getText().toString());
    }

    @OnClick(R.id.btn_another_test)
    void anotherTest() {
        finish();
    }

    @OnClick(R.id.btn_next_patient)
    void nextPatient() {
        Intent newIntent = new Intent(this, ActivityDashboard.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
    }

    @OnClick(R.id.btn_print_report)
    void printReport() {

    }
}