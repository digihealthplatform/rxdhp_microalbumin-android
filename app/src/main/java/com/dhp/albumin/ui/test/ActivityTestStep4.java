package com.dhp.albumin.ui.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivityTest;

import static com.dhp.albumin.data.manager.SharedPrefsHelper.WAIT_DURATION;
import static com.dhp.albumin.utils.LConstants.DEFAULT_WAIT_DURATION;
import static com.dhp.albumin.utils.LConstants.TYPE_CALIBRATE;
import static com.dhp.albumin.utils.LConstants.TYPE_TEST;

public class ActivityTestStep4 extends BaseAppCompatActivityTest {

    private int timeCounter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_seconds)
    TextView tvSeconds;

    @BindView(R.id.btn_next)
    Button btnNext;

    @Inject
    DataManager dataManager;

    private Timer timer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_step_4);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        initToolbar(toolbar, getString(R.string.title_albumin_test), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        timeCounter = dataManager.getIntPref(WAIT_DURATION, DEFAULT_WAIT_DURATION);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                if (timeCounter == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvSeconds.setText("" + timeCounter);
                            btnNext.setVisibility(View.VISIBLE);
                        }
                    });

                    timer.cancel();
                    timer.purge();

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvSeconds.setText("" + timeCounter);
                            --timeCounter;
                        }
                    });
                }
            }
        }, 0, 1000);
    }

    @OnClick(R.id.btn_next)
    void next() {
        Intent intent = new Intent(this, ActivityCameraView.class);
        intent.putExtra("activity_type", TYPE_TEST);
        startActivity(intent);
        finish();
    }
}