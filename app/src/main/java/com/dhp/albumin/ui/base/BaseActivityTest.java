package com.dhp.albumin.ui.base;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;

import com.dhp.albumin.R;

public class BaseActivityTest extends Activity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return false;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        confirmBackPressed();
    }

    private void confirmBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setMessage(getString(R.string.message_confirm_on_back));
        builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BaseActivityTest.super.onBackPressed();
            }
        });

        builder.setNegativeButton(getString(R.string.btn_no), null);
        builder.show();
    }
}