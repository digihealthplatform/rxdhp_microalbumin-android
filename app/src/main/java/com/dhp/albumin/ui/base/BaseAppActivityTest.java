package com.dhp.albumin.ui.base;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.dhp.albumin.LApplication;
import com.dhp.albumin.R;
import com.dhp.albumin.di.component.ActivityComponent;
import com.dhp.albumin.di.component.DaggerActivityComponent;
import com.dhp.albumin.di.module.ActivityModule;
import com.dhp.albumin.utils.LLog;

public class BaseAppActivityTest extends Activity {
    protected ActivityComponent activityComponent;

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(LApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                confirmBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        confirmBackPressed();
    }

    protected void confirmBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.message_confirm_on_back));
        builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BaseAppActivityTest.super.onBackPressed();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_no), null);
        builder.show();
    }
}