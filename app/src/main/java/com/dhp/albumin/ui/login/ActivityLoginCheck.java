package com.dhp.albumin.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.ui.dashboard.ActivityDashboard;

public class ActivityLoginCheck extends BaseAppCompatActivity {

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent(this).inject(this);

        Intent intent;

        if (dataManager.isLoggedIn()) {
            intent = new Intent(this, ActivityDashboard.class);
        } else {
            intent = new Intent(this, ActivityLogin.class);
        }

        startActivity(intent);

        finish();
    }
}