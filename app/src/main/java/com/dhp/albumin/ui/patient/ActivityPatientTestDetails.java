package com.dhp.albumin.ui.patient;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TablePatient;
import com.dhp.albumin.data.table.TableTest;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.utils.LUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.albumin.utils.LUtils.getResultColor;

public class ActivityPatientTestDetails extends BaseAppCompatActivity {

    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_albumin_value)
    TextView tvAlbuminValue;

    @BindView(R.id.tv_diagnosis)
    TextView tvDiagnosis;

    @BindView(R.id.tv_albumin_value_with_correction)
    TextView tvAlbuminValueCorrection;

    @BindView(R.id.tv_diagnosis_with_correction)
    TextView tvDiagnosisCorrection;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_age)
    TextView tvAge;

    @BindView(R.id.tv_gender)
    TextView tvGender;

    @BindView(R.id.tv_phone)
    TextView tvPhone;

    @BindView(R.id.tv_test_date)
    TextView tvTestDate;

    @BindView(R.id.view_result_color)
    View viewResultColor;

    private int testId;
    private int patientId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_details);
        initToolbar(toolbar, getString(R.string.title_test_details), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        if (savedInstanceState != null) {
            testId = savedInstanceState.getInt("test_id");
            patientId = savedInstanceState.getInt("patient_id");
        } else {
            testId = getIntent().getIntExtra("test_id", 0);
            patientId = getIntent().getIntExtra("patient_id", 0);
        }

        TablePatient tablePatient = dataManager.getPatient(patientId);
        tvName.setText(tablePatient.getFullName());
        tvAge.setText("" + tablePatient.getAge());
        tvGender.setText(tablePatient.getGender());
        tvPhone.setText(tablePatient.getPhone());

        TableTest tableTest = dataManager.getTest(testId);
        tvTestDate.setText(LUtils.getFormattedDateTime(tableTest.getTestDateTime()));

        tvDiagnosis.setText(tableTest.getDiagnosis());
        tvDiagnosisCorrection.setText(tableTest.getDiagnosisCorrection());
        viewResultColor.setBackgroundColor(getResultColor(Float.parseFloat(tableTest.getAlbuminValue())));

        if (dataManager.getBooleanPref("show_albumin", false)) {
            tvAlbuminValue.setText("Albumin Value: " + tableTest.getAlbuminValue());
            tvAlbuminValueCorrection.setText("Albumin Value: " + tableTest.getAlbuminValueCorrection());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("test_id", testId);
        outState.putInt("patient_id", patientId);
        super.onSaveInstanceState(outState);
    }
}