package com.dhp.albumin.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TableUser;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.ui.dashboard.ActivityDashboard;
import com.dhp.albumin.utils.LConstants;

public class ActivityLogin extends BaseAppCompatActivity {

    @BindView(R.id.et_user_name)
    EditText etUserName;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.tv_login_error)
    TextView tvLoginError;

    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    private final int REQUEST_CODE = 1099;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initToolbar(toolbar, getString(R.string.title_login), false);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        createFoldersWithPermission();
    }

    private void createFoldersWithPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE);

        } else {
            createFolders();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createFolders();
                } else {
                    Toast.makeText(this, "Please allow the permissions", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
            break;
        }
    }

    private void createFolders() {
        File folder = new File(Environment.getExternalStorageDirectory(), LConstants.IMAGE_PATH);

        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    @OnTouch(R.id.et_user_name)
    boolean touchUserName() {
        tvLoginError.setVisibility(View.GONE);
        return false;
    }

    @OnTouch(R.id.et_password)
    boolean touchPassword() {
        tvLoginError.setVisibility(View.GONE);
        return false;
    }

    @OnClick(R.id.tv_forgot_password)
    void forgotPassword() {
        tvLoginError.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_login)
    void login() {
        tvLoginError.setVisibility(View.GONE);

        if (dataManager.verifyUserCredentials(etUserName.getText().toString(),
                etPassword.getText().toString())) {
            finish();
            startActivity(new Intent(this, ActivityDashboard.class));
            overridePendingTransition(0, 0);
            dataManager.setLoggedIn(true, etUserName.getText().toString());
        } else {
            tvLoginError.setVisibility(View.VISIBLE);
        }
    }
}