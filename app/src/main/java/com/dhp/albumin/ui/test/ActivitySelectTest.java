package com.dhp.albumin.ui.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;

public class ActivitySelectTest extends BaseAppCompatActivity {

    private int patientId;

    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_test);
        initToolbar(toolbar, getString(R.string.title_select_test), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        if (savedInstanceState != null) {
            patientId = savedInstanceState.getInt("patient_id");
        } else {
            patientId = getIntent().getIntExtra("patient_id", 0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("patient_id", patientId);
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.btn_test_albumin)
    void startTest() {
        Intent intent = new Intent(this, ActivityTestStep1.class);
        dataManager.setCurrentTestPatientId(patientId);
        dataManager.setCurrentTestType("Albumin/Protein");
        startActivity(intent);
        finish();
    }
}