package com.dhp.albumin.ui.statistics;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;

public class ActivityStatistics extends BaseAppCompatActivity {

    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_total_users)
    TextView tvTotalUsers;
    @BindView(R.id.tv_total_patients)
    TextView tvTotalPatients;
    @BindView(R.id.tv_total_tests)
    TextView tvTotalTests;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        initToolbar(toolbar, getString(R.string.title_statistics), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        tvTotalUsers.setText("" + dataManager.getAllUsers().size());
        tvTotalPatients.setText("" + dataManager.getAllPatients().size());
        tvTotalTests.setText("" + dataManager.getAllTests().size());
    }
}