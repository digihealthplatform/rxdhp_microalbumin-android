package com.dhp.albumin.ui.test.renalyx;

import android.graphics.Bitmap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class JsonParserNew {


    public static String manufacturer_name;

    public static String names[] = new String[100];
    public static int count;
    public static String pname[] = new String[50];
    public static String color[] = new String[50];
    public static String[] value = new String[50];
    public static String[] images_color_code;
    public static Bitmap[] captured_images;

    HashMap<String, String> mapData = new HashMap<String, String>();

    public static List<Map<String, String>> mapTable = new ArrayList<Map<String, String>>();

    public JsonParserNew() {
    }


    public JsonParserNew(String jsonstring, String selected_name) {
        // TODO Auto-generated constructor stub

        System.out.println("manav in new json parser constructor");

        int pos = 0;

        mapTable = new ArrayList<Map<String, String>>();
        mapData = new HashMap<String, String>();
        names = new String[100];
        pname = new String[50];
        color = new String[50];
        value = new String[50];


        try {
            // Creating JSONObject from String
            JSONObject jsonObjMain = new JSONObject(jsonstring);

            // Creating JSONArray from JSONObject
            JSONArray jsonArray = jsonObjMain.getJSONArray("manufacturers");
            System.out.println("manav in try :" + jsonArray.toString());

            // JSONArray has four JSONObject
            for (int i = 0; i < jsonArray.length(); i++) {

                // Creating JSONObject from JSONArray
                JSONObject jsonObj = jsonArray.getJSONObject(i);

                // Getting data from individual JSONObject

                if (jsonObj.getString("name").equals(selected_name)) {

                    manufacturer_name = jsonObj.getString("name");

                    count = jsonObj.getInt("paramcount");

                    System.out.println("manav count" + count);

                    JSONArray jsonsubArray = jsonObj.getJSONArray("parameters");

                    for (int j = 0; j < jsonsubArray.length(); j++) {

                        JSONObject jsonsubObj = jsonsubArray.getJSONObject(j);


                        // Getting data from individual JSONObject
                        System.out.println("manav " + jsonsubObj.getString("pname"));
                        pname[j] = jsonsubObj.getString("pname");
                        System.out.println(jsonsubObj.getString("color1"));

                        mapData = new HashMap<String, String>();

                        mapData.put(jsonsubObj.getString("color1"), jsonsubObj.getString("value1"));
                        mapData.put(jsonsubObj.getString("color2"), jsonsubObj.getString("value2"));
                        mapData.put(jsonsubObj.getString("color3"), jsonsubObj.getString("value3"));
                        mapData.put(jsonsubObj.getString("color4"), jsonsubObj.getString("value4"));
                        mapData.put(jsonsubObj.getString("color5"), jsonsubObj.getString("value5"));
                        mapData.put(jsonsubObj.getString("color6"), jsonsubObj.getString("value6"));

                        /*Map<String, String> treeMap = new TreeMap<String, String>(mapData);

                        mapData.clear();
                        Set set2 = treeMap.entrySet();
                        Iterator iterator2 = set2.iterator();
                        while(iterator2.hasNext()) {
                            Map.Entry me2 = (Map.Entry)iterator2.next();
                            System.out.print("manav sort");
                            System.out.print(me2.getKey() + ": ");
                            System.out.println(me2.getValue());
                            mapData.put(me2.getKey().toString() , me2.getValue().toString());
                        }
*/

                        for (Map.Entry<String, String> entry : mapData.entrySet()) {
                            System.out.println("manav map : " + pname[j] + " Key = " + entry.getKey() + ", Value = " + entry.getValue());
                        }


                        mapTable.add(mapData);

                      /*  color[pos] = jsonsubObj.getString("color1");
                        value[pos]= jsonsubObj.getString("value1");
                        pos = pos+1;
                        color[pos] = jsonsubObj.getString("color2");
                        value[pos] = jsonsubObj.getString("value2");
                        pos = pos+1;
                        color[pos] = jsonsubObj.getString("color3");
                        value[pos] = jsonsubObj.getString("value3");
                        pos = pos+1;
                        color[pos] = jsonsubObj.getString("color4");
                        value[pos] = jsonsubObj.getString("value4");
                        pos = pos+1;
                        color[pos] = jsonsubObj.getString("color5");
                        value[pos] = jsonsubObj.getString("value5");
                        pos = pos+1;
                        color[pos] = jsonsubObj.getString("color6");
                        value[pos] = jsonsubObj.getString("value6");

                        pos = pos+1;
*/


                    }
                }//if
            }

            // Append result to TextView
            System.out.println("Manav New json Parser : " + mapTable.size());


            for (int i = 0; i < mapTable.size(); i++) {

                for (Map.Entry<String, String> entry : mapTable.get(i).entrySet()) {
                    System.out.println(i + " Key = " + entry.getKey() + ", Value = " + entry.getValue());
                }


            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }


    public String[] getManufacturernames(String jsonnameString) {

        try {

            JSONObject jsonObjMain = new JSONObject(jsonnameString);

            // Creating JSONArray from JSONObject
            JSONArray jsonArray = jsonObjMain.getJSONArray("manufacturers");

            System.out.println("manav" + jsonArray.length());
            // JSONArray has four JSONObject
            for (int i = 0; i < jsonArray.length(); i++) {

                // Creating JSONObject from JSONArray
                JSONObject jsonObj = jsonArray.getJSONObject(i);

                // Getting data from individual JSONObject
                names[i] = jsonObj.getString("name");
            }
            System.out.println("name " + names.length);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //return names;
        return names;


    }

    public void setimages(Bitmap[] images) {

        Bitmap[] bitmap = new Bitmap[10];
        bitmap = images;

        int cnt = 0;

        int i = 0;
        while (bitmap[i] != null) {

            cnt++;
            i++;

        }

        captured_images = new Bitmap[cnt];
        System.arraycopy(bitmap, 0, captured_images, 0, cnt);


    }

    public void setimagescolor(String[] images) {

        String[] bitmap_color = new String[10];

        bitmap_color = images;

        int cnt = 0;

        int i = 0;
        while (bitmap_color[i] != null) {

            cnt++;
            i++;

        }

        images_color_code = new String[cnt];
        System.arraycopy(bitmap_color, 0, images_color_code, 0, cnt);


    }


}
