package com.dhp.albumin.ui.base;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.dhp.albumin.LApplication;
import com.dhp.albumin.R;
import com.dhp.albumin.di.component.ActivityComponent;
import com.dhp.albumin.di.component.DaggerActivityComponent;
import com.dhp.albumin.di.module.ActivityModule;
import com.dhp.albumin.utils.LLog;

public class BaseAppCompatActivity extends AppCompatActivity {
    protected ActivityComponent activityComponent;

    protected void initToolbar(Toolbar toolbar, String title, boolean setHomeUpEnabled) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);

        setSupportActionBar(toolbar);

        if (setHomeUpEnabled) {
            try {
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (NullPointerException e) {
                LLog.printStackTrace(e);
            }
        }
    }

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(LApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}