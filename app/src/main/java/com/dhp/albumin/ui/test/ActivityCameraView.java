package com.dhp.albumin.ui.test;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.item.ItemAlbumin;
import com.dhp.albumin.opencv.ImageProcessing;
import com.dhp.albumin.ui.base.BaseAppActivityTest;
import com.dhp.albumin.ui.test.renalyx.Resultclass;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.albumin.utils.LConstants.IMAGE_PATH;
import static com.dhp.albumin.utils.LConstants.TYPE_CALIBRATE;
import static com.dhp.albumin.utils.LConstants.TYPE_TEST;
import static com.dhp.albumin.utils.LConstants.TYPE_TEST_URINE;


public class ActivityCameraView extends BaseAppActivityTest implements CameraBridgeViewBase.CvCameraViewListener2 {

    private int currentContrast = 0;
    private int currentIso      = 0;

    private int activityType;

    private final int CONTRAST_MIN = 0;
    private final int CONTRAST_MAX = 6;
    private final int ISO_MIN      = 0;
    private       int ISO_MAX      = 6;

    private String[] isoValues;

    private static final String TAG = "ActivityCameraView";

    private Mat rgba;
    private Bitmap[] bitmap = new Bitmap[8];

    private ProgressDialog progressDialog;

    private AlertDialog waitDialog;
    private View        dialogView;

    @BindView(R.id.jc_view)
    JavaCameraView jcView;

    @BindView(R.id.btn_iso_plus)
    Button btnIsoPlus;

    @BindView(R.id.btn_iso_minus)
    Button btnIsoMinus;

    @BindView(R.id.tv_iso_value)
    TextView tvIsoValue;

    @BindView(R.id.btn_contrast_plus)
    Button btnContrastPlus;

    @BindView(R.id.btn_contrast_minus)
    Button btnContrastMinus;

    @BindView(R.id.tv_contrast_value)
    TextView tvContrastValue;

    @BindView(R.id.btn_calibrate)
    Button btnCalibrate;

    @BindView(R.id.btn_capture)
    Button btnCapture;

    @BindView(R.id.ll_contrast)
    LinearLayout llContrast;

    @BindView(R.id.ll_saturation)
    LinearLayout llSaturation;

    private HttpURLConnection connection;
    private Context           context;

    String serverResponseMessage;
    int    serverResponseCode;

    long requestLength;

    @Inject
    DataManager dataManager;

    int pendingTime  = 3;
    int captureCount = 0;

    // First Capture
    File imgLRef1;

    // Second capture -  After dipping in urine
    File imgLRef2_1, imgLRef2_2, imgLRef2_3, imgLTest_1, imgLTest_2, imgLTest_3, calibrationFile;

    private boolean takeAverage;

    private BaseLoaderCallback loaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    jcView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera_view);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        activityType = getIntent().getIntExtra("activity_type", 0);

        takeAverage = dataManager.getBooleanPref("take_average", true);

        if (TYPE_TEST_URINE == activityType || TYPE_TEST == activityType) {
            btnCalibrate.setVisibility(View.GONE);
            llContrast.setVisibility(View.GONE);
            llSaturation.setVisibility(View.GONE);

        } else {
            btnCapture.setVisibility(View.GONE);
        }

        jcView.setVisibility(SurfaceView.VISIBLE);
        jcView.setCvCameraViewListener(this);

        jcView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            }
        });

        btnContrastPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContrastMinus.setEnabled(true);

                ++currentContrast;

                if (CONTRAST_MAX <= currentContrast) {
                    currentContrast = CONTRAST_MAX;
                    btnContrastPlus.setEnabled(false);
                } else {
                    btnContrastPlus.setEnabled(true);
                }

                jcView.setContrast(currentContrast);

                dataManager.setPref("contrast", currentContrast);
                tvContrastValue.setText("" + currentContrast);
            }
        });

        btnContrastMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContrastPlus.setEnabled(true);

                --currentContrast;

                if (CONTRAST_MIN >= currentContrast) {
                    btnContrastMinus.setEnabled(false);
                    currentContrast = CONTRAST_MIN;
                } else {
                    btnContrastMinus.setEnabled(true);
                }

                jcView.setContrast(currentContrast);

                dataManager.setPref("contrast", currentContrast);
                tvContrastValue.setText("" + currentContrast);
            }
        });

        btnIsoPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnIsoMinus.setEnabled(true);

                ++currentIso;

                if (ISO_MAX <= currentIso) {
                    currentIso = ISO_MAX;
                    btnIsoPlus.setEnabled(false);
                } else {
                    btnIsoPlus.setEnabled(true);
                }

                jcView.setIso(isoValues[currentIso]);
                dataManager.setPref("iso", currentIso);

                tvIsoValue.setText(isoValues[currentIso]);
            }
        });

        btnIsoMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnIsoPlus.setEnabled(true);

                --currentIso;

                if (ISO_MIN >= currentIso) {
                    currentIso = ISO_MIN;
                    btnIsoMinus.setEnabled(false);
                } else {
                    btnIsoMinus.setEnabled(true);
                }

                jcView.setIso(isoValues[currentIso]);

                dataManager.setPref("iso", currentIso);
                tvIsoValue.setText(isoValues[currentIso]);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, loaderCallback);
        } else {
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        rgba = new Mat(height, width, CvType.CV_8UC4);

        currentContrast = dataManager.getIntPref("contrast", -1);

        if (-1 == currentContrast) {
            currentContrast = jcView.getContrast();
        }

        jcView.setContrast(currentContrast);

        isoValues = jcView.getIsoValues().split(",");

        ISO_MAX = isoValues.length;

        btnIsoMinus.setEnabled(false);

        tvContrastValue.setText("" + currentContrast);

        currentIso = dataManager.getIntPref("iso", 0);

        jcView.setIso(isoValues[currentIso]);
        tvIsoValue.setText("" + isoValues[currentIso]);

        if (CONTRAST_MIN >= currentContrast) {
            btnContrastMinus.setEnabled(false);
            currentContrast = CONTRAST_MIN;
        } else {
            btnContrastMinus.setEnabled(true);
        }

        if (ISO_MIN >= currentIso) {
            currentIso = ISO_MIN;
            btnIsoMinus.setEnabled(false);
        } else {
            btnIsoMinus.setEnabled(true);
        }
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        rgba = inputFrame.rgba();

        if (TYPE_TEST_URINE == activityType || TYPE_TEST == activityType) {
            //capture
            Point tl = new Point(rgba.width() * 0.26, rgba.height() * 0.45);
            Point br = new Point(rgba.width() * 0.28, rgba.height() * 0.49);

            Scalar scalar1;
            Scalar scalar2;

            if (TYPE_TEST_URINE == activityType) {
                scalar1 = new Scalar(255, 255, 255);
                scalar2 = new Scalar(255, 255, 0);

            } else {
                scalar1 = new Scalar(255, 255, 255);
                scalar2 = new Scalar(0, 0, 255);
            }

            Imgproc.rectangle(rgba, tl, br, scalar1, 5);
//            Imgproc.rectangle(rgba, tl, br, new Scalar(255, 0, 0), 5);
            Imgproc.rectangle(rgba, new Point(tl.x, tl.y + rgba.height() * 0.09),
                    new Point(br.x, br.y + rgba.height() * 0.09), scalar2, 5);

            //Imgproc.rectangle(rgba, new Point(tl.x ,tl.y + rgba.height()*0.09), new Point(br.x ,br.y + rgba.height()*0.09), new Scalar(255, 0, 0), 5);

            return rgba;
        } else {
            Point tl = new Point(rgba.width() * 0.15, rgba.height() * 0.1);
            Point br = new Point(rgba.width() * 0.3, rgba.height() * 0.9);
            Imgproc.rectangle(rgba, tl, br, new Scalar(255, 0, 0), 5);
            return rgba;
        }
    }

    @Override
    public void onCameraViewStopped() {

    }

    private void count() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                --pendingTime;

                if (0 == pendingTime) {

                    if (TYPE_TEST == activityType && takeAverage) {
                        capture();
                        captureCount++;
                        if (captureCount < 3) {
                            pendingTime = 3;
                            count();
                        }
                    } else {
                        capture();
                    }
                } else {
                    ((TextView) dialogView.findViewById(R.id.tv_wait)).setText("" + pendingTime);
                    count();
                }
            }
        }, 1000);
    }

    @OnClick({R.id.btn_calibrate, R.id.btn_capture})
    void onCapture() {
        captureCount = 0;

        dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_wait, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setTitle("Wait...");
        builder.setView(dialogView);

        waitDialog = builder.create();
        waitDialog.setCanceledOnTouchOutside(false);
        waitDialog.show();
        ((TextView) dialogView.findViewById(R.id.tv_wait)).setText("" + pendingTime);

        count();
    }

    void capture() {
        // Dismiss loader

        if (takeAverage && TYPE_CALIBRATE != activityType) {
            if (2 <= captureCount) {
                waitDialog.dismiss();
            }
        } else {
            waitDialog.dismiss();
        }

        int x      = 0;
        int y      = 0;
        int width  = rgba.width();
        int height = rgba.height();
        Mat paramRegion;
        Mat paramRegion1;

        if (TYPE_TEST == activityType || TYPE_TEST_URINE == activityType) {
            //capture
            Point tl    = new Point(rgba.width() * 0.26, rgba.height() * 0.45);
            Point br    = new Point(rgba.width() * 0.28, rgba.height() * 0.49);
            Rect  crop  = new Rect(tl, br);
            Rect  crop1 = new Rect(new Point(tl.x, tl.y + rgba.height() * 0.09), new Point(br.x, br.y + rgba.height() * 0.09));

            paramRegion = rgba.submat(crop);
            paramRegion1 = rgba.submat(crop1);
            bitmap[1] = Bitmap.createBitmap(paramRegion1.cols(), paramRegion1.rows(), Bitmap.Config.ARGB_8888); // The white non reactive pad
            Utils.matToBitmap(paramRegion1, bitmap[1]);
        } else {
            Point tl   = new Point(rgba.width() * 0.15, rgba.height() * 0.1);
            Point br   = new Point(rgba.width() * 0.3, rgba.height() * 0.9);
            Rect  crop = new Rect(tl, br);

            paramRegion = rgba.submat(crop);
        } //The above 'else' piece of code is implemented during calibration

        bitmap[0] = Bitmap.createBitmap(paramRegion.cols(), paramRegion.rows(), Bitmap.Config.ARGB_8888);   //Either the reactive pad of the strip or calibration image
        Utils.matToBitmap(paramRegion, bitmap[0]);

        if (TYPE_TEST_URINE == activityType) {
            imgLRef1 = saveTempImage(bitmap[0], "img_lref1_"); // White Pad
            processFile();

        } else if (TYPE_TEST == activityType) {

            if (takeAverage) {
                switch (captureCount) {
                    case 0:
                        imgLRef2_1 = saveTempImage(bitmap[0], "img_lref2_1_");
                        imgLTest_1 = saveTempImage(bitmap[1], "img_ltest_1_");
                        break;
                    case 1:
                        imgLRef2_2 = saveTempImage(bitmap[0], "img_lref2_2_");
                        imgLTest_2 = saveTempImage(bitmap[1], "img_ltest_2_");
                        break;
                    case 2:
                        imgLRef2_3 = saveTempImage(bitmap[0], "img_lref2_3_");
                        imgLTest_3 = saveTempImage(bitmap[1], "img_ltest_3_");
                        break;
                }

                if (captureCount == 2) {
                    processFile();
                }
            } else {
                imgLRef2_1 = saveTempImage(bitmap[0], "img_lref2_1_");
                imgLTest_1 = saveTempImage(bitmap[1], "img_ltest_1_");
                processFile();
            }
        } else {
            calibrationFile = saveTempImage(bitmap[0], "calibration_");
            processFile();
        }
    }

    private void processFile() {
        showLoader();

        new Thread(new Runnable() {
            @Override
            public void run() {
//                uploadFile(imgFile.getAbsolutePath());

                if (TYPE_TEST_URINE == activityType) {
                    // No need to process it now
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finishTask(imgLRef1.getName());
                        }
                    });

                } else if (TYPE_TEST == activityType) {
                    ItemAlbumin itemAlbumin;
                    try {

                        if (takeAverage) {
                            itemAlbumin = ImageProcessing.findAlbumin(
                                    Environment.getExternalStorageDirectory() + "/" + IMAGE_PATH + "/" + dataManager.getStringPref("lref_1", ""),
                                    imgLRef2_1.getPath(),
                                    imgLRef2_2.getPath(),
                                    imgLRef2_3.getPath(),
                                    Environment.getExternalStorageDirectory() + "/" + IMAGE_PATH + "/" + dataManager.getStringPref("calibration_file", ""),
                                    imgLTest_1.getPath(),
                                    imgLTest_2.getPath(),
                                    imgLTest_3.getPath()
                            );
                        } else {
                            itemAlbumin = ImageProcessing.findAlbumin(
                                    Environment.getExternalStorageDirectory() + "/" + IMAGE_PATH + "/" + dataManager.getStringPref("lref_1", ""),
                                    imgLRef2_1.getPath(),
                                    Environment.getExternalStorageDirectory() + "/" + IMAGE_PATH + "/" + dataManager.getStringPref("calibration_file", ""),
                                    imgLTest_1.getPath());
                        }

                        Resultclass.albumin = itemAlbumin.getAlbumin();
                        Resultclass.albumin_correction = itemAlbumin.getAlbuminCorrection();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                finishTask("");
                            }
                        });

                    } catch (Exception e) {
                        Log.d(TAG, "run: ------------------exception");
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pendingTime = 5;
                                progressDialog.dismiss();
                                Toast.makeText(ActivityCameraView.this, "Unable to process. Try again", Toast.LENGTH_SHORT).show();
                            }
                        });
//                        LLog.printStackTrace(e);
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finishTask(calibrationFile.getName());
                        }
                    });
                }
            }
        }).start();
    }

    private void showLoader() {
        progressDialog = new ProgressDialog(this, R.style.AlertDialogLight);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    private void finishTask(String fileName) {
        progressDialog.dismiss();

        if (TYPE_TEST_URINE == activityType) {
            dataManager.setPref("lref_1", fileName);
            startActivity(new Intent(ActivityCameraView.this, ActivityTestStep2.class));

        } else if (TYPE_TEST == activityType) {
            startActivity(new Intent(ActivityCameraView.this, ActivityTestReport.class));

        } else {
            dataManager.setPref("calibration_file", fileName);
            Toast.makeText(ActivityCameraView.this, "Calibrated", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    private File saveTempImage(Bitmap bitmapImage, String name) {
        File folder = new File(Environment.getExternalStorageDirectory(), IMAGE_PATH);

        SimpleDateFormat sdf  = new SimpleDateFormat("dd_MM_yyyy-HH_mm_ss");
        Date             date = new Date();

        String fileName = name + sdf.format(date) + "-iso_" + isoValues[currentIso] + "-C_" + currentContrast + ".png";
        File   imgFile  = new File(folder, fileName);

        try {
            FileOutputStream fos = new FileOutputStream(imgFile);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgFile;
    }

    public void onDestroy() {
        super.onDestroy();

        if (jcView != null)
            jcView.disableView();
    }
}