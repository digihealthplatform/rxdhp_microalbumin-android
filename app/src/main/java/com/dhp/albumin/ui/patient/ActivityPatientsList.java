package com.dhp.albumin.ui.patient;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import com.dhp.albumin.R;
import com.dhp.albumin.adapter.AdapterPatientsList;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;

public class ActivityPatientsList extends BaseAppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_search_name)
    EditText etSearchName;

    @BindView(R.id.iv_search)
    ImageView ivSearch;

    @BindView(R.id.tv_empty)
    TextView tvTvEmpty;

    @BindView(R.id.rv_patients_list)
    RecyclerView rvPatientsList;

    @Inject
    DataManager dataManager;

    private InputMethodManager mInputMethodManager;

    private AdapterPatientsList adapterPatientsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_list);
        initToolbar(toolbar, getString(R.string.title_patients_list), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        adapterPatientsList = new AdapterPatientsList(this, dataManager, tvTvEmpty);
        mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        etSearchName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapterPatientsList.onSearchTextChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @OnClick(R.id.iv_search)
    void search() {
        adapterPatientsList.onSearchTextChanged(etSearchName.getText().toString());
        mInputMethodManager.hideSoftInputFromWindow(etSearchName.getWindowToken(), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        rvPatientsList.setLayoutManager(new LinearLayoutManager(this));
        rvPatientsList.setAdapter(adapterPatientsList);

        if (0 == dataManager.getAllPatients().size()) {
            rvPatientsList.setVisibility(View.GONE);
            tvTvEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mInputMethodManager.hideSoftInputFromWindow(etSearchName.getWindowToken(), 0);
        etSearchName.setText("");
    }
}