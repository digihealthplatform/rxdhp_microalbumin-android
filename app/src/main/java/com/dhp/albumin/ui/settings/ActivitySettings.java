package com.dhp.albumin.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.ui.dashboard.ActivityDashboard;
import com.dhp.albumin.utils.LConstants;
import com.dhp.albumin.utils.LToast;

import static com.dhp.albumin.data.manager.SharedPrefsHelper.CAMPAIGN_NAME;
import static com.dhp.albumin.data.manager.SharedPrefsHelper.STRIP_CODE;
import static com.dhp.albumin.data.manager.SharedPrefsHelper.WAIT_DURATION;

public class ActivitySettings extends BaseAppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_strip_code)
    EditText etStripCode;

    @BindView(R.id.et_campaign_name)
    EditText etCampaignName;

    @BindView(R.id.et_wait_dutation)
    EditText etWaitDuration;

    @BindView(R.id.cb_take_average)
    CheckBox cbTakeAverage;

    @BindView(R.id.cb_show_albumin_value)
    CheckBox cbShowAlbumin;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initToolbar(toolbar, getString(R.string.title_settings), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                etStripCode.setText(dataManager.getStringPref(STRIP_CODE, LConstants.DEFAULT_STRIP_CODE));
                etCampaignName.setText(dataManager.getStringPref(CAMPAIGN_NAME, LConstants.DEFAULT_CAMPAIGN_NAME));
                etWaitDuration.setText("" + dataManager.getIntPref(WAIT_DURATION, LConstants.DEFAULT_WAIT_DURATION));

            }
        }, 250);

        cbTakeAverage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dataManager.setPref("take_average", b);
            }
        });

        cbShowAlbumin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dataManager.setPref("show_albumin", b);
            }
        });

        cbTakeAverage.setChecked(dataManager.getBooleanPref("take_average", true));
        cbShowAlbumin.setChecked(dataManager.getBooleanPref("show_albumin", true));
    }

    @OnClick(R.id.btn_add_user)
    void addUser() {
        startActivity(new Intent(this, ActivityAddUser.class));
    }

    @OnClick(R.id.btn_save)
    void save() {
        if (etStripCode.getText().toString().equals("")) {
            LToast.warning(R.string.error_strip_code);
            return;
        }

        if (etCampaignName.getText().toString().equals("")) {
            LToast.warning(R.string.error_campaign_name);
            return;
        }

        int waitDuration = 0;

        try {
            waitDuration = Integer.parseInt(etWaitDuration.getText().toString());
        } catch (Exception e) {

        }
        if (0 == waitDuration) {
            LToast.warning(R.string.error_wait_duration);
            return;
        }

        dataManager.setPref(STRIP_CODE, etStripCode.getText().toString());
        dataManager.setPref(CAMPAIGN_NAME, etCampaignName.getText().toString());
        dataManager.setPref(WAIT_DURATION, waitDuration);

        LToast.success(R.string.settngs_updated);
    }
}