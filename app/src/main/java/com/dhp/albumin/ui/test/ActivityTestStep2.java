package com.dhp.albumin.ui.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.albumin.R;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.ui.base.BaseAppCompatActivityTest;

public class ActivityTestStep2 extends BaseAppCompatActivityTest {

    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar   toolbar;
    @BindView(R.id.iv_dip_stick)
    ImageView ivDipStick;

    private TranslateAnimation mAnimation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_step_2);
        initToolbar(toolbar, getString(R.string.title_albumin_test), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        animateDipStick();
    }

    private void animateDipStick() {
        ivDipStick.setVisibility(View.VISIBLE);

        mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f);

        mAnimation.setDuration(1500);
        mAnimation.setRepeatCount(-1);
        mAnimation.setStartOffset(1000);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        ivDipStick.setAnimation(mAnimation);
    }

    @OnClick(R.id.btn_next)
    void next() {
        Intent intent = new Intent(this, ActivityTestStep3.class);
        startActivity(intent);
        finish();
    }
}