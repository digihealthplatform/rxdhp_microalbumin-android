package com.dhp.albumin.ui.help;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.dhp.albumin.R;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;

public class ActivityHelp extends BaseAppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        initToolbar(toolbar, getString(R.string.title_help), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);
    }
}