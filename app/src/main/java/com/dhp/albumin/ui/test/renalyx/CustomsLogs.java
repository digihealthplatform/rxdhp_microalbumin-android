package com.dhp.albumin.ui.test.renalyx;

import android.os.Environment;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomsLogs {

    public static final boolean Debug = true;
    public static final boolean User_Events = true & Debug;
    public static final boolean System_op_Events = true & Debug;
    public static final boolean System_ip_Events = true & Debug;
    public static final boolean Event_Command = true & Debug;
    public static final boolean Event_Data = true & Debug;

    public static File file;

    public static BufferedOutputStream buf2;
    public static BufferedWriter buf;


    //	public static void write_to_file(String id,String hexcolor,String rgbcolor,String intcolor ,String labcolor, String time , String reflab , String caplab , String result){
    public static void write_to_file(String time) {

        //		String pattern = id+"\t"+hexcolor+"\t"+rgbcolor+"\t"+intcolor+"\t"+labcolor+"\t"+time+"\t"+reflab+"\t"+caplab+"\t"+result+"\n";
        String pattern = Resultclass.id + "\t" + Resultclass.colour + "\t" + time + "\t" + Resultclass.refLab + "\t" + Resultclass.capLab + "\t" + Resultclass.albuminvalue + "\t" + Resultclass.result + "\n";

        try {

            //	buf = new BufferedWriter(new FileWriter(file, true));

            buf.write(pattern);
            //	buf.newLine();
            buf.flush();

            //	buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public static void filecreation() {

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yy h_mm_ss a");

        file = new File(Environment
                .getExternalStorageDirectory() + "/urinelogs", sdf.format(date) + ".txt");
        if (!file.exists())
            try {

                file.createNewFile();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        try {

            buf = new BufferedWriter(new FileWriter(file, true));
//				String header = "id"+"\t"+"Colorhex"+"\t"+"Colorrgb"+"\t"+"Colorint"+"\t"+"Colorlab"+"\t"+"Time"+"\t"+"RefL*"+"\t"+"CapL*"+"\t"+"Result"+"\n";
            String header = "id" + "\t" + "Colorhex" + "\t" + "Time" + "\t" + "RefL*" + "\t" + "CapL*" + "\t" + "Result Value" + "\t" + "Result" + "\n";

            buf.write(header);
            //	buf.newLine();
            buf.flush();

            //	buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


}
