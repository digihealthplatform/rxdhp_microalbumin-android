package com.dhp.albumin.ui.test;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.albumin.R;
import com.dhp.albumin.ui.base.BaseActivityTest;
import com.dhp.albumin.ui.test.renalyx.JsonParserNew;
import com.dhp.albumin.ui.test.renalyx.SelectionNew;
import com.dhp.albumin.utils.LLog;
import com.dhp.albumin.ui.test.renalyx.Resultclass;

public class ActivityCapture2 extends BaseActivityTest implements CameraBridgeViewBase.CvCameraViewListener2 {

    private boolean mIsJavaCamera = true;
    private MenuItem mItemSwitchCamera = null;
    Rect rect1, pHrect, albrect, refrect;
    private Rect[] rects = new Rect[5];
    private Bitmap[] bitmap = new Bitmap[5];
    int noOfparameters = 2;
    String[] strColor = new String[10];

    private Mat rgba;

    @BindView(R.id.jcv_main)
    JavaCameraView javaCameraView;

    private BaseLoaderCallback loaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    javaCameraView.enableView();
                }
                break;

                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);
        ButterKnife.bind(this);

        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);




        javaCameraView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
//                    javaCameraView.focusOnTouch(event);
                }
                return true;

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, loaderCallback);
        } else {
            Log.d("", "OpenCV library found inside package. Using it!");
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        rgba = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        rgba = inputFrame.rgba();

        Rect rect = new Rect();
        rect.width = inputFrame.rgba().width();
        rect.height = inputFrame.rgba().height();

        int x = (int) (rect.tl().x + rect.br().x) / 2 + 200;
        int y = (int) (rect.tl().y + rect.br().y) / 2;

        rect1 = new Rect(x, y, 400, 90);

        int widthOffset = 15;
        int startingGap = 40;
        int inbetweenGap = 50;
        int heightOffset = 15;

        int startingX = x + startingGap;
        int startingY = y + heightOffset;

        int totalWidth = 35;
        int totalHeight = 30;

        for (int i = 0; i < noOfparameters; i++) {
            rects[i] = new Rect(startingX, startingY, totalWidth, totalHeight);

            Imgproc.rectangle(rgba, rects[i].tl(), rects[i].br(), new Scalar(255, 0, 0), 2, 8, 0);

            startingX = startingX + totalWidth + inbetweenGap + widthOffset;
        }

        Imgproc.rectangle(rgba, rect1.tl(), rect1.br(), new Scalar(0, 0, 0), 2, 8, 0);

        return rgba;
    }

    @OnClick(R.id.btn_capture)
    void capture() {
        Bitmap bmp = null;

        try {
            Mat selectedRegion = rgba.submat(rect1);
            bmp = Bitmap.createBitmap(selectedRegion.cols(), selectedRegion.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(selectedRegion, bmp);

            for (int i = 0; i < noOfparameters; i++) {
                Mat paramRegion = rgba.submat(rects[i]);
                bitmap[i] = Bitmap.createBitmap(paramRegion.cols(), paramRegion.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(paramRegion, bitmap[i]);
                captureColor(bitmap[i], i);
            }

            saveToInternalSorage(bitmap[0], 1);

            JsonParserNew jp = new JsonParserNew();
            jp.setimages(bitmap);
            jp.setimagescolor(strColor);

            Intent intent = new Intent(this, SelectionNew.class);
            startActivity(intent);
            finish();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public void captureColor(Bitmap bmp, int count) {
        int offset = 1;
        int red = 0;
        int green = 0;
        int blue = 0;
        int color = 0;
        int pixelsNumber = 0;
        int[] rgb = new int[10];
        int xImage = bmp.getWidth() / 2;
        int yImage = bmp.getHeight() / 2;

        for (int i = 0; i <= bmp.getWidth(); i++) {
            for (int j = 0; j <= bmp.getHeight(); j++) {
                try {
                    color = bmp.getPixel(i, j);
                    red += Color.red(color);
                    green += Color.green(color);
                    blue += Color.blue(color);
                    pixelsNumber += 1;
                } catch (Exception e) {
                    LLog.printStackTrace(e);
                }
            }
        }
        red = red / pixelsNumber;
        green = green / pixelsNumber;
        blue = blue / pixelsNumber;


        int rgb2 = red;
        rgb2 = (rgb2 << 8) + green;
        rgb2 = (rgb2 << 8) + blue;

        strColor[count] = String.format("#%06X", 0xFFFFFF & rgb2);
    }

    private void saveToInternalSorage(Bitmap bitmapImage, int option) {
        String filename = null;

        String random = Resultclass.id;

        switch (option) {
            case 1:
                filename = String.valueOf(random) + "Color.png";
                break;
            case 2:
                filename = String.valueOf(random) + "Ref.png";
                break;
        }

        File myFile = new File(Environment.getExternalStorageDirectory() + "/albumin-test");

        if (!myFile.isDirectory()) {
            myFile.mkdir();
        }

        myFile = new File(Environment.getExternalStorageDirectory() + "/albumin-test", filename);

        if (!myFile.exists()) {
            try {
                myFile.createNewFile();
            } catch (IOException e) {
                LLog.printStackTrace(e);
            }
        }

        try {
            FileOutputStream fos = new FileOutputStream(myFile);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }
}