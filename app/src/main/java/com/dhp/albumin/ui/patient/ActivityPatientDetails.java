package com.dhp.albumin.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.dhp.albumin.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.table.TablePatient;
import com.dhp.albumin.ui.base.BaseAppCompatActivity;
import com.dhp.albumin.ui.test.ActivitySelectTest;

public class ActivityPatientDetails extends BaseAppCompatActivity {

    private int patientId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_dob)
    TextView tvDob;
    @BindView(R.id.tv_age)
    TextView tvAge;
    @BindView(R.id.tv_gender)
    TextView tvGender;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        initToolbar(toolbar, getString(R.string.title_patient_details), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        if (savedInstanceState != null) {
            patientId = savedInstanceState.getInt("patient_id");
        } else {
            patientId = getIntent().getIntExtra("patient_id", 0);
        }

        TablePatient tablePatient = dataManager.getPatient(patientId);

        tvName.setText(tablePatient.getFullName());
        tvDob.setText(tablePatient.getDob());
        tvAge.setText("" + tablePatient.getAge());
        tvGender.setText(tablePatient.getGender());
        tvPhone.setText(tablePatient.getPhone());
        tvAddress.setText(tablePatient.getAddress());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("patient_id", patientId);
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.btn_start_test)
    void startTest() {
        Intent intent = new Intent(this, ActivitySelectTest.class);
        intent.putExtra("patient_id", patientId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_view_tests)
    void viewTests() {
        Intent intent = new Intent(this, com.dhp.albumin.ui.patient.ActivityPatientTestsList.class);
        intent.putExtra("patient_id", patientId);
        startActivity(intent);
    }
}