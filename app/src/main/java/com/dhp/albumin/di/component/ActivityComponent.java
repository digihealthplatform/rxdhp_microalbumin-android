package com.dhp.albumin.di.component;

import dagger.Component;

import com.dhp.albumin.di.PerActivity;
import com.dhp.albumin.di.module.ActivityModule;
import com.dhp.albumin.ui.dashboard.ActivityDashboard;
import com.dhp.albumin.ui.help.ActivityHelp;
import com.dhp.albumin.ui.login.ActivityLogin;
import com.dhp.albumin.ui.login.ActivityLoginCheck;
import com.dhp.albumin.ui.patient.ActivityPatientDetails;
import com.dhp.albumin.ui.patient.ActivityPatientTestDetails;
import com.dhp.albumin.ui.patient.ActivityPatientTestsList;
import com.dhp.albumin.ui.patient.ActivityPatientsList;
import com.dhp.albumin.ui.settings.ActivityAddUser;
import com.dhp.albumin.ui.settings.ActivitySettings;
import com.dhp.albumin.ui.statistics.ActivityStatistics;
import com.dhp.albumin.ui.test.ActivityCameraView;
import com.dhp.albumin.ui.test.ActivitySelectTest;
import com.dhp.albumin.ui.test.ActivityCapture1;
import com.dhp.albumin.ui.test.ActivityCapture2;
import com.dhp.albumin.ui.test.ActivityTestReport;
import com.dhp.albumin.ui.test.ActivityTestStep1;
import com.dhp.albumin.ui.test.ActivityTestStep2;
import com.dhp.albumin.ui.test.ActivityTestStep3;
import com.dhp.albumin.ui.test.ActivityTestStep4;

@PerActivity
@Component(dependencies = com.dhp.albumin.di.component.ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(ActivityLoginCheck activityLoginCheck);

    void inject(ActivityLogin activityLogin);

    void inject(ActivityDashboard activityDashboard);

    void inject(ActivityPatientDetails activityPatientDetails);

    void inject(ActivityPatientsList activityPatientsList);

    void inject(ActivityPatientTestsList activityPatientTestsList);

    void inject(ActivityPatientTestDetails activityPatientTestDetails);

    void inject(ActivitySelectTest activitySelectTest);

    void inject(ActivityTestReport activityTestReport);

    void inject(ActivityTestStep1 activityTestStep1);

    void inject(ActivityTestStep2 activityTestStep2);

    void inject(ActivityTestStep3 activityTestStep3);

    void inject(ActivityTestStep4 activityTestStep4);

    void inject(ActivityCapture1 activityCapture1);

    void inject(ActivityCapture2 activityCapture2);

    void inject(ActivitySettings activitySettings);

    void inject(ActivityAddUser activityAddUser);

    void inject(ActivityHelp activityHelp);

    void inject(ActivityStatistics activityStatistics);

    void inject(ActivityCameraView activityCameraView);
}