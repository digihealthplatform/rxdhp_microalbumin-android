package com.dhp.albumin.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import com.dhp.albumin.LApplication;
import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.data.manager.SharedPrefsHelper;
import com.dhp.albumin.di.ApplicationContext;
import com.dhp.albumin.di.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(LApplication lApplication);

    @ApplicationContext
    Context getContext();

    DataManager getDataManager();
}