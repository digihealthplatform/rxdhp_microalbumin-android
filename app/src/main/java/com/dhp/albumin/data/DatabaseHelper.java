package com.dhp.albumin.data;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = DatabaseHelper.NAME, version = DatabaseHelper.VERSION)
public class DatabaseHelper {
    public static final String NAME = "DatabaseHelper";
    public static final int VERSION = 1;
}