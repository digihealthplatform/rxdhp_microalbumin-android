package com.dhp.albumin.data.manager;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SharedPrefsHelper {

    public static final String IS_USER_LOGGED_IN       = "IS_USER_LOGGED_IN";
    public static final String LOGGED_IN_USER_ID       = "LOGGED_IN_USER_ID";
    public static final String CURRENT_TEST_PATIENT_ID = "CURRENT_TEST_PATIENT_ID";
    public static final String CURRENT_TEST_TYPE       = "CURRENT_TEST_TYPE";

    public static final String IS_FIRST_TIME_OPENING_APP = "IS_FIRST_TIME_OPENING_APP";
    public static final String STRIP_CODE                = "STRIP_CODE";
    public static final String CAMPAIGN_NAME             = "CAMPAIGN_NAME";
    public static final String WAIT_DURATION             = "wait_duration";

    private SharedPreferences sharedPreferences;

    @Inject
    public SharedPrefsHelper(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setPref(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public void setPref(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void setPref(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public int getIntPref(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public boolean getBooleanPref(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public String getStringPref(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }
}