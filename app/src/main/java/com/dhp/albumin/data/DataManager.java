package com.dhp.albumin.data;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.dhp.albumin.data.manager.DatabaseManager;
import com.dhp.albumin.data.manager.SharedPrefsHelper;
import com.dhp.albumin.data.table.TablePatient;
import com.dhp.albumin.data.table.TableTest;
import com.dhp.albumin.data.table.TableUser;
import com.dhp.albumin.di.ApplicationContext;

import static com.dhp.albumin.data.manager.SharedPrefsHelper.CURRENT_TEST_PATIENT_ID;
import static com.dhp.albumin.data.manager.SharedPrefsHelper.CURRENT_TEST_TYPE;
import static com.dhp.albumin.data.manager.SharedPrefsHelper.IS_USER_LOGGED_IN;
import static com.dhp.albumin.data.manager.SharedPrefsHelper.LOGGED_IN_USER_ID;

@Singleton
public class DataManager {
    private Context           context;
    private DatabaseManager   databaseManager;
    private SharedPrefsHelper sharedPrefsHelper;

    @Inject
    public DataManager(@ApplicationContext Context context,
                       DatabaseManager databaseManager,
                       SharedPrefsHelper sharedPrefsHelper) {
        this.context = context;
        this.databaseManager = databaseManager;
        this.sharedPrefsHelper = sharedPrefsHelper;
    }

    public boolean createUser(TableUser tableUser) {
        return databaseManager.createUser(tableUser);
    }

    public TableUser getUser(String userName) {
        return databaseManager.getUser(userName);
    }

    public boolean createPatient(TablePatient tablePatient) {
        return databaseManager.createPatient(tablePatient);
    }

    public TablePatient getPatient(int patientId) {
        return databaseManager.getPatient(patientId);
    }

    public boolean createTest(TableTest tableTest) {
        return databaseManager.createTest(tableTest);
    }

    public TableTest getTest(int testId) {
        return databaseManager.getTest(testId);
    }

    public int getPatientId(String phone) {
        return databaseManager.getPatientId(phone);
    }

    public List<TablePatient> getAllPatients() {
        return databaseManager.getAllPatients();
    }

    public List<TablePatient> getAllPatients(String queryString) {
        return databaseManager.getAllPatients(queryString);
    }

    public List<TableUser> getAllUsers() {
        return databaseManager.getAllUsers();
    }

    public List<TableTest> getAllTests() {
        return databaseManager.getAllTests();
    }

    public List<TableTest> getAllTests(int patientId) {
        return databaseManager.getAllTests(patientId);
    }

    public boolean verifyUserCredentials(String userName, String password) {
        return databaseManager.verifyUserCredentials(userName, password);
    }

    public boolean isLoggedIn() {
        return sharedPrefsHelper.getBooleanPref(IS_USER_LOGGED_IN, false);
    }

    public void setLoggedIn(boolean loggedIn, String userId) {
        sharedPrefsHelper.setPref(IS_USER_LOGGED_IN, loggedIn);
        sharedPrefsHelper.setPref(LOGGED_IN_USER_ID, userId);
    }

    public String getLoggedInUserId() {
        return sharedPrefsHelper.getStringPref(LOGGED_IN_USER_ID, "");
    }

    public void setPref(String key, int value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public void setPref(String key, boolean value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public void setPref(String key, String value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public int getIntPref(String key, int defaultValue) {
        return sharedPrefsHelper.getIntPref(key, defaultValue);
    }

    public boolean getBooleanPref(String key, boolean defaultValue) {
        return sharedPrefsHelper.getBooleanPref(key, defaultValue);
    }

    public String getStringPref(String key, String defaultValue) {
        return sharedPrefsHelper.getStringPref(key, defaultValue);
    }

    public void setCurrentTestPatientId(int patientId) {
        sharedPrefsHelper.setPref(CURRENT_TEST_PATIENT_ID, patientId);
    }

    public int getCurrentTestPatientId() {
        return sharedPrefsHelper.getIntPref(CURRENT_TEST_PATIENT_ID, -1);
    }

    public void setCurrentTestType(String testType) {
        sharedPrefsHelper.setPref(CURRENT_TEST_TYPE, testType);
    }

    public String getCurrentTestType() {
        return sharedPrefsHelper.getStringPref(CURRENT_TEST_TYPE, "");
    }
}