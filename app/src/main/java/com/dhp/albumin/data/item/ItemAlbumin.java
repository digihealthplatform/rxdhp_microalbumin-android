package com.dhp.albumin.data.item;

public class ItemAlbumin {

    private float albumin;
    private float albuminCorrection;

    public float getAlbumin() {
        return albumin;
    }

    public void setAlbumin(float albumin) {
        this.albumin = albumin;
    }

    public float getAlbuminCorrection() {
        return albuminCorrection;
    }

    public void setAlbuminCorrection(float albuminCorrection) {
        this.albuminCorrection = albuminCorrection;
    }
}
