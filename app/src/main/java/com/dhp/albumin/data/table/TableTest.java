package com.dhp.albumin.data.table;

import com.dhp.albumin.data.DatabaseHelper;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

@Table(database = DatabaseHelper.class)
public class TableTest extends BaseModel implements Serializable {

    @Column
    @PrimaryKey(autoincrement = true)
    private int testId;

    @Column
    private int patientId;

    @Column
    private String userId;

    @Column
    private String testType;

    @Column
    private String albuminValue;

    @Column
    private String albuminValueCorrection;

    @Column
    private String diagnosis;

    @Column
    private String diagnosisCorrection;

    @Column
    private long testDateTime;

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public String getAlbuminValue() {
        return albuminValue;
    }

    public void setAlbuminValue(String albuminValue) {
        this.albuminValue = albuminValue;
    }

    public String getAlbuminValueCorrection() {
        return albuminValueCorrection;
    }

    public void setAlbuminValueCorrection(String albuminValueCorrection) {
        this.albuminValueCorrection = albuminValueCorrection;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getDiagnosisCorrection() {
        return diagnosisCorrection;
    }

    public void setDiagnosisCorrection(String diagnosisCorrection) {
        this.diagnosisCorrection = diagnosisCorrection;
    }

    public long getTestDateTime() {
        return testDateTime;
    }

    public void setTestDateTime(long testDateTime) {
        this.testDateTime = testDateTime;
    }
}