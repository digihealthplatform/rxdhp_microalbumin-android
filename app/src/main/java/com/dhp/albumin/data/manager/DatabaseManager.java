package com.dhp.albumin.data.manager;

import android.content.Context;

import com.dhp.albumin.data.table.TablePatient_Table;
import com.dhp.albumin.data.table.TableTest;
import com.dhp.albumin.data.table.TableTest_Table;
import com.dhp.albumin.data.table.TableUser;
import com.dhp.albumin.data.table.TableUser_Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.dhp.albumin.data.table.TablePatient;
import com.dhp.albumin.di.ApplicationContext;
import com.dhp.albumin.di.DatabaseInfo;
import com.dhp.albumin.utils.LLog;


@Singleton
public class DatabaseManager {

    private Context context;

    @Inject
    DatabaseManager(@ApplicationContext Context context,
                    @DatabaseInfo String dbName,
                    @DatabaseInfo Integer version) {
        this.context = context;
    }

    public boolean createUser(TableUser tableUser) {
        try {
            tableUser.save();
            return true;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public boolean createPatient(TablePatient tablePatient) {
        try {
            tablePatient.save();
            return true;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public boolean createTest(TableTest tableTest) {
        try {
            tableTest.save();
            return true;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public List<TableUser> getAllUsers() {
        try {
            return SQLite.select().from(TableUser.class).queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public List<TableTest> getAllTests() {
        try {
            return SQLite.select().from(TableTest.class).queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public List<TableTest> getAllTests(int patientId) {
        try {
            return SQLite.select().from(TableTest.class).where(TableTest_Table.patientId.eq(patientId))
                    .orderBy(TableTest_Table.testDateTime, false).queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public List<TablePatient> getAllPatients() {
        try {
            return SQLite.select().from(TablePatient.class)
                    .orderBy(TablePatient_Table.fullName, true).queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public List<TablePatient> getAllPatients(String queryString) {
        try {
            return SQLite.select()
                    .from(TablePatient.class)
                    .where(TablePatient_Table.fullName.like(queryString + "%"))
                    .or(TablePatient_Table.phone.like(queryString + "%"))
                    .orderBy(TablePatient_Table.fullName, true)
                    .queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public TableUser getUser(String userName) {
        try {
            return SQLite.select().from(TableUser.class).where(TableUser_Table.userName.eq(userName)).querySingle();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TableUser();
    }

    public TablePatient getPatient(int patientId) {
        try {
            return SQLite.select().from(TablePatient.class).where(TablePatient_Table.patientId.eq(patientId)).querySingle();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TablePatient();
    }

    public TableTest getTest(int testId) {
        try {
            return SQLite.select().from(TableTest.class).where(TableTest_Table.testId.eq(testId)).querySingle();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TableTest();
    }

    public int getPatientId(String phone) {
        try {
            return SQLite.select().from(TablePatient.class).where(TablePatient_Table.phone.eq(phone)).querySingle().getPatientId();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return -1;
    }

    public boolean verifyUserCredentials(String userName, String password) {
        try {
            List<TableUser> tableUsers = SQLite.select()
                    .from(TableUser.class)
                    .where(TableUser_Table.userName.eq(userName)).and(TableUser_Table.password.eq(password))
                    .queryList();

            if (0 < tableUsers.size()) {
                return true;
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }
}