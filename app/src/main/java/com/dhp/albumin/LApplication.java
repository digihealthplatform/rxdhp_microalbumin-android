package com.dhp.albumin;

import android.app.Application;
import android.content.Context;

import com.dhp.albumin.data.table.TableUser;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import com.dhp.albumin.data.DataManager;
import com.dhp.albumin.di.component.ApplicationComponent;
import com.dhp.albumin.di.component.DaggerApplicationComponent;
import com.dhp.albumin.di.module.ApplicationModule;
import com.dhp.albumin.ui.test.renalyx.JsonParserNew;

import static com.dhp.albumin.data.manager.SharedPrefsHelper.IS_FIRST_TIME_OPENING_APP;

public class LApplication extends Application {

    private static LApplication instance;

    protected ApplicationComponent applicationComponent;

    @Inject
    DataManager dataManager;

    public static LApplication get(Context context) {
        return (LApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);

        FlowManager.init(new FlowConfig.Builder(this).build());

        if (dataManager.getBooleanPref(IS_FIRST_TIME_OPENING_APP, true)) {
            initAppValues();
            dataManager.setPref(IS_FIRST_TIME_OPENING_APP, false);
        }

        calibrate();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public static Context getContext() {
        return instance;
    }

    private void initAppValues() {
        TableUser user = new TableUser();
        user.setFullName("Test User 1");
        user.setUserName("u1");
        user.setPassword("p1");
        dataManager.createUser(user);
    }

    private void calibrate() {
        JSONObject parent = new JSONObject();
        JSONObject obj    = new JSONObject();
        JSONArray  req    = new JSONArray();
        JSONObject reqObj = new JSONObject();

        try {
            reqObj.put("pname", "Albumin");

/*
        reqObj.put( "color1", 4 );
		reqObj.put( "value1", "30.0" );

		reqObj.put( "color2", 10 );
		reqObj.put( "value2", "100.0" );

		reqObj.put( "color3", 14 );
		reqObj.put( "value3", "200.0" );

		reqObj.put( "color4", 16 );
		reqObj.put( "value4", "300.0" );

		reqObj.put( "color5", 24 );
		reqObj.put( "value5", "400.0" );

		reqObj.put( "color6", 30 );
		reqObj.put( "value6", "500.0" );
*/

            reqObj.put("color1", 7);
            reqObj.put("value1", "30.0");

            reqObj.put("color2", 9);
            reqObj.put("value2", "100.0");

            reqObj.put("color3", 14);
            reqObj.put("value3", "150.0");

            reqObj.put("color4", 20);
            reqObj.put("value4", "300.0");

            reqObj.put("color5", 22);
            reqObj.put("value5", "400.0");

            reqObj.put("color6", 24);
            reqObj.put("value6", "500.0");

            req.put(reqObj);

            obj.put("name", "b1");
            obj.put("paramcount", 1);
            obj.put("parameters", req);

            JSONArray mainarr = new JSONArray();

            mainarr.put(obj);

            parent.put("manufacturers", mainarr);

            new JsonParserNew(parent.toString(), "b1");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}