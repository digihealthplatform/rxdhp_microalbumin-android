package com.dhp.albumin.opencv;

import android.graphics.Bitmap;
import android.os.Environment;

import com.dhp.albumin.data.item.ItemAlbumin;
import com.opencsv.CSVWriter;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static org.opencv.imgproc.Imgproc.boundingRect;

public class ImageProcessing {

    private static final String TAG = "ImageProcessing";

    private static int[]  RESULT_VALUES = new int[]{10, 30, 80, 150, 300, 500};
    private static double affinity[]    = {0.1, 0.2, 0.3, 0.7, 0.85, 1};

    private static class Values {

        public Values(double first, double sec) {
            this.firstValue = first;
            this.secondValue = sec;
        }

        public Values(double[] array, double first) {
            this.firstValue = first;
            this.array = array;
        }

        public Values(double[] array, double[] array2, double[] array3) {
            this.array = array;
            this.array2 = array2;
            this.array3 = array3;
        }

        double[] array3;
        double[] array2;
        double[] array;
        double   firstValue;
        double   secondValue;
    }

    //Three images are captured - 1. Calibration, 2. White strip image, 3. Urine sample image
    public static ItemAlbumin findAlbumin(String imgLRef1, String imgLRef2, String calibrationFileName, String captureUrineFileName) throws Exception {
        Mat matLRef1       = Imgcodecs.imread(imgLRef1);
        Mat matLRef2       = Imgcodecs.imread(imgLRef2);
        Mat matCalibration = Imgcodecs.imread(calibrationFileName);
        Mat matTest        = Imgcodecs.imread(captureUrineFileName);

        Bitmap tempcap = Bitmap.createBitmap(matLRef1.cols(), matLRef1.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(matLRef1, tempcap);

//        Bitmap tempur = Bitmap.createBitmap(urine.cols(), urine.rows(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(urine, tempur);

        float albumin = 0;

        // Will now have to return the LAB values from getLvalues. Store the L, A and B values in separate arrays
        ValueArray values_calibration = getLValues(matCalibration);

        Double[]     calibrationLvalues = values_calibration.Lshades;
        List<Double> LValues            = Arrays.asList(calibrationLvalues);
        Collections.reverse(LValues);
        calibrationLvalues = LValues.toArray(calibrationLvalues);

        Double[]     calibrationAshades = values_calibration.Avalues;
        List<Double> AValues            = Arrays.asList(calibrationAshades);
        Collections.reverse(AValues);
        calibrationAshades = AValues.toArray(calibrationAshades);

        Double[]     calibrationBshades = values_calibration.Bvalues;
        List<Double> BValues            = Arrays.asList(calibrationBshades);
        Collections.reverse(BValues);
        calibrationBshades = BValues.toArray(calibrationBshades);

        values_calibration.Lshades = calibrationLvalues;
        values_calibration.Avalues = calibrationAshades;
        values_calibration.Bvalues = calibrationBshades;

        double avgLRef1  = getLvalue(matLRef1, false).Lshades;
        double avgLRef2  = getLvalue(matLRef2, false).Lshades;
        double deltaLRef = Math.abs(avgLRef1 - avgLRef2); // is just the L value enough for yellow correction

        // Will now have to return the LAB value for getLvalue. Store the L, A and B values in separate variables
        // This is for the urine image file, where is the captured image file?
        Value valTest = getLvalue(matTest, true);

        int index = findClosest(valTest, values_calibration);

        valTest.Lshades += deltaLRef;

        int indexCorrection = findClosest(valTest, values_calibration);

        albumin = RESULT_VALUES[index];
        float albuminCorrection = RESULT_VALUES[indexCorrection];

        ItemAlbumin itemAlbumin = new ItemAlbumin();
        itemAlbumin.setAlbumin(albumin);
        itemAlbumin.setAlbuminCorrection(albuminCorrection);

        return itemAlbumin;
    }

    public static ItemAlbumin findAlbumin(String imgLRef1,
                                          String imgLRef2_1,
                                          String imgLRef2_2,
                                          String imgLRef2_3,
                                          String calibrationFileName,
                                          String imgLTest_1,
                                          String imgLTest_2,
                                          String imgLTest_3) throws Exception {

        Mat matLRef1 = Imgcodecs.imread(imgLRef1);

        Mat matLRef2_1 = Imgcodecs.imread(imgLRef2_1);
        Mat matLRef2_2 = Imgcodecs.imread(imgLRef2_2);
        Mat matLRef2_3 = Imgcodecs.imread(imgLRef2_3);

        Mat matCalibration = Imgcodecs.imread(calibrationFileName);

        Mat matLTest_1 = Imgcodecs.imread(imgLTest_1);
        Mat matLTest_2 = Imgcodecs.imread(imgLTest_2);
        Mat matLTest_3 = Imgcodecs.imread(imgLTest_3);

//        Bitmap tempcap = Bitmap.createBitmap(captured.cols(), captured.rows(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(captured, tempcap);

//        Bitmap tempur = Bitmap.createBitmap(urine.cols(), urine.rows(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(urine, tempur);

        float albumin = 0;

        //Seems pretty redundant, not being used at all--> both vals, calibration() and cap_val_min and cap_val_max
//        Values vals        = calibration();
//        double cap_val_min = vals.firstValue;
//        double cap_val_max = vals.secondValue;

        ValueArray values_calibration = getLValues(matCalibration);
        //Reversing the order of the captured calibration LAB values --> corresponding to 10 to 500
        Double[]     calibrationLvalues = values_calibration.Lshades;
        List<Double> LValues            = Arrays.asList(calibrationLvalues);
        Collections.reverse(LValues);
        calibrationLvalues = LValues.toArray(calibrationLvalues);

        Double[]     calibrationAshades = values_calibration.Avalues;
        List<Double> AValues            = Arrays.asList(calibrationAshades);
        Collections.reverse(AValues);
        calibrationAshades = AValues.toArray(calibrationAshades);

        Double[]     calibrationBshades = values_calibration.Bvalues;
        List<Double> BValues            = Arrays.asList(calibrationBshades);
        Collections.reverse(BValues);
        calibrationBshades = BValues.toArray(calibrationBshades);

        values_calibration.Lshades = calibrationLvalues;
        values_calibration.Avalues = calibrationAshades;
        values_calibration.Bvalues = calibrationBshades;

        // Need to question its requirement - changed on 3/1/2019
        //double[] Lvalues     = getLValues(calibration);
        //Arrays.sort(Lvalues);

//        vals = correction(Lvalues, cap_val_min, cap_val_max);
//        double[] range_val           = vals.array;
//        double[] correction_factor   = vals.array2;
//        double[] current_shades_norm = vals.array3;

//        double Lblood1 = getLvalue(captured1);
//        double Lblood2 = getLvalue(captured2);
//        double Lblood3 = getLvalue(captured3);

        // Added code on 3/1/2019

        Value valTest1 = getLvalue(matLTest_1, true);
        Value valTest2 = getLvalue(matLTest_2, true);
        Value valTest3 = getLvalue(matLTest_3, true);

        double avgLTest = (valTest1.Lshades + valTest2.Lshades + valTest3.Lshades) / 3.0;
        double avgATest = (valTest1.Avalues + valTest2.Avalues + valTest3.Avalues) / 3.0;
        double avgBTest = (valTest1.Bvalues + valTest2.Bvalues + valTest3.Bvalues) / 3.0;

        Value valLRef2_1 = getLvalue(matLRef2_1, false);
        Value valLRef2_2 = getLvalue(matLRef2_2, false);
        Value valLRef2_3 = getLvalue(matLRef2_3, false);

        double avgLRef1  = getLvalue(matLRef1, false).Lshades;
        double avgLRef2  = (valLRef2_1.Lshades + valLRef2_2.Lshades + valLRef2_3.Lshades) / 3.0;
        double deltaLRef = Math.abs(avgLRef1 - avgLRef2); // is just the L value enough for yellow correction

        Value Urine_reading = new Value();
        Urine_reading.Lshades = avgLTest;
        Urine_reading.Avalues = avgATest;
        Urine_reading.Bvalues = avgBTest;

        int index = findClosest(Urine_reading, values_calibration);

        Urine_reading.Lshades = avgLTest + deltaLRef;
        int indexCorrection = findClosest(Urine_reading, values_calibration); // Will need to see how to implement Yellow correction

        //albumin = RESULT_VALUES[RESULT_VALUES.length - 1 - index];

        albumin = RESULT_VALUES[index];
        float albuminWithCorrection = RESULT_VALUES[indexCorrection];

        ItemAlbumin itemAlbumin = new ItemAlbumin();
        itemAlbumin.setAlbumin(albumin);
        itemAlbumin.setAlbuminCorrection(albuminWithCorrection);

        // Export without delta correction
        Urine_reading.Lshades = avgLTest - deltaLRef;
        exportCsv(RESULT_VALUES, values_calibration, valTest1, valTest2, valTest3, albumin);

        return itemAlbumin;
    }

    private static int findClosest(Value value, ValueArray valuesList) {
        int    index          = 0;
        Double Lshades_sample = value.Lshades;
        Double Ashades_sample = value.Avalues;
        Double Bshades_sample = value.Bvalues;

        Double[] Lvalues_cal = valuesList.Lshades;
        Double[] Avalues_cal = valuesList.Avalues;
        Double[] Bvalues_cal = valuesList.Bvalues;

        //todo, temporary fix
        Lvalues_cal[4] = Lvalues_cal[3] - 2.0d;

        if (Lshades_sample > Lvalues_cal[0]) {
            index = 0;
        } else {
            for (int i = 0; i < 5; i++) {
                index = i;
                if ((Lvalues_cal[i] >= Lshades_sample) && (Lshades_sample > Lvalues_cal[i + 1])) {
                    double diffPrev = sqrt((pow((Lshades_sample - Lvalues_cal[i]), 2)) +
                            (pow((Ashades_sample - Avalues_cal[i]), 2)) +
                            (pow((Bshades_sample - Bvalues_cal[i]), 2))); // * affinity[i-1];
                    //double diffPrev = (value - valuesList[i - 1])*affinity[i-1];
                    double diffNext = sqrt((pow((Lshades_sample - Lvalues_cal[i + 1]), 2)) +
                            (pow((Ashades_sample - Avalues_cal[i + 1]), 2)) +
                            (pow((Bshades_sample - Bvalues_cal[i + 1]), 2))); // * affinity[i];
                    //double diffNext = abs(valuesList[i] - value)*affinity[i];

                    if (diffPrev < diffNext) {
                        index = i;
                    } else if (diffPrev > diffNext) {
                        index = i + 1;
                    } else {
                        index = i + 1;
                    }
                    break;
                } else {
                    index = i + 1;
                }
            }
        }
/*        for (int i = 0; i < Lvalues_cal.length; i++) {
            index = i;

            if (Lshades_sample <= Lvalues_cal[i]) {
                if (0 == i) {
                    index = i;
                    break;
                }
                else {

                    double  diffPrev    =   sqrt((pow((Lshades_sample - Lvalues_cal[i-1]),2)) +
                                            (pow((Ashades_sample - Avalues_cal[i-1]),2)) +
                                            (pow((Bshades_sample - Bvalues_cal [i-1]),2))); // * affinity[i-1];
                    //double diffPrev = (value - valuesList[i - 1])*affinity[i-1];
                    double  diffNext    =   sqrt((pow((Lshades_sample - Lvalues_cal[i]),2)) +
                                            (pow((Ashades_sample - Avalues_cal[i]),2)) +
                                            (pow((Bshades_sample - Bvalues_cal [i]),2))); // * affinity[i];
                    //double diffNext = abs(valuesList[i] - value)*affinity[i];

                    if (diffPrev < diffNext) {
                        index = i - 1;
                    } else if (diffPrev > diffNext) {
                        index = i;
                    } else {
                        index = i;
                    }

                    break;
                }
            }
        }*/

        return index;
    }

    private static int findClosest(double value, Double[] valuesList) {
        int index = 0;

        for (int i = 0; i < 4; i++) {
            index = i;

            if (value <= valuesList[i]) {
                if (0 == i) {
                    index = i;
                    break;
                } else {
                    double diffPrev = (value - valuesList[i - 1]) * affinity[i - 1];
                    double diffNext = (valuesList[i] - value) * affinity[i];

                    if (diffPrev < diffNext) {
                        index = i - 1;
                    } else if (diffPrev > diffNext) {
                        index = i;
                    } else {
                        index = i - 1;
                    }

                    break;
                }
            }
        }

        return index;
    }

    private static double find_c_factor(double[] current_shades_norm, double[] correction_factor, double norm) {
        double c = 0;
        if (norm <= current_shades_norm[1]) {
            c = correction_factor[0];
        } else if (current_shades_norm[1] < norm && norm <= current_shades_norm[2]) {
            c = correction_factor[1];
        } else if (current_shades_norm[2] < norm && norm <= current_shades_norm[3]) {
            c = correction_factor[2];
        } else if (current_shades_norm[3] < norm && norm <= current_shades_norm[4]) {
            c = correction_factor[3];
        } else if (current_shades_norm[4] < norm && norm <= current_shades_norm[5]) {
            c = correction_factor[4];
        } else if (current_shades_norm[5] < norm) {
            c = correction_factor[5];
        }
        return c;
    }

    private static Value getLvalue(Mat bgr_image, boolean isGreen) throws Exception {
        Size   size = bgr_image.size();
        double M    = size.height;
        double N    = size.width;

        Mat rgb_image = new Mat(size, CvType.CV_8UC1);
        Imgproc.cvtColor(bgr_image, rgb_image, Imgproc.COLOR_BGR2RGB);  //Storing an RGB image

        //Mat crop_img = bgr_image.submat(75, (int) (M), 50, (int) (N - 450));

        Mat smooth    = new Mat(size, CvType.CV_8UC1);
        Mat lab_image = new Mat(size, CvType.CV_8UC1);

        //Imgproc.blur(bgr_image, smooth, new Size(5, 5));
        Imgproc.cvtColor(bgr_image, lab_image, Imgproc.COLOR_BGR2Lab);  //Converting to Lab color space
        return getLblood(bgr_image, lab_image, isGreen);
    }

    private static Value getLblood(Mat crop_img, Mat lab_image, boolean isGreen) throws Exception {
        Size size        = crop_img.size();
        Mat  median_blur = new Mat(size, CvType.CV_8UC1);
        Mat  gray        = new Mat(size, CvType.CV_8UC1);
        Mat  circles     = new Mat(size, CvType.CV_8UC1);
        Mat  mask        = new Mat(size, CvType.CV_8UC1, new Scalar(0));

        Mat     hsv_image           = new Mat(size, CvType.CV_8UC1);
        Mat     lower_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat     upper_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat     red_hue_image       = new Mat(size, CvType.CV_8UC1);
        Point   center              = new Point();
        float[] radii               = new float[1];
        int     radius;

        Mat closing    = new Mat(size, CvType.CV_8UC1);
        Mat kernel_9x9 = new Mat(new Size(5, 5), CvType.CV_8UC1, new Scalar(1));

        Imgproc.cvtColor(crop_img, hsv_image, Imgproc.COLOR_BGR2HSV);

        if (isGreen) {
            Core.inRange(hsv_image, new Scalar(39, 20, 30), new Scalar(100, 255, 255), lower_red_hue_range);
            Core.inRange(hsv_image, new Scalar(25, 50, 30), new Scalar(39, 255, 255), upper_red_hue_range);
            Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);

            Bitmap tempdisp6 = Bitmap.createBitmap(red_hue_image.cols(), red_hue_image.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(red_hue_image, tempdisp6);
        } else {
            Core.inRange(hsv_image, new Scalar(0, 0, 0), new Scalar(255, 255, 255), lower_red_hue_range);
            Core.inRange(hsv_image, new Scalar(0, 0, 0), new Scalar(255, 255, 255), upper_red_hue_range);
            Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);

            Bitmap tempdisp6 = Bitmap.createBitmap(red_hue_image.cols(), red_hue_image.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(red_hue_image, tempdisp6);
        }

        Mat hierarchy = new Mat();

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.morphologyEx(red_hue_image, closing, Imgproc.MORPH_ERODE, kernel_9x9);

        Imgproc.findContours(closing, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        MatOfPoint2f contour0 = new MatOfPoint2f(contours.get(0).toArray());
        Imgproc.minEnclosingCircle(contour0, center, radii);
        radius = (int) (radii[0]) - 7;
        Imgproc.circle(mask, center, radius, new Scalar(255, 255, 255), -1);


//        Imgproc.medianBlur(crop_img, median_blur, 5);
//        Imgproc.cvtColor(median_blur, gray, Imgproc.COLOR_BGR2GRAY);
//        Imgproc.HoughCircles(gray, circles, Imgproc.HOUGH_GRADIENT, 1, 20, 50, 30, 0, 0);
//
//        int numberOfCircles = (circles.rows() == 0) ? 0 : circles.cols();
//        for (int i = 0; i < numberOfCircles; i++) {
//            double[] circleCoordinates = circles.get(0, i);
//            int x = (int) circleCoordinates[0], y = (int) circleCoordinates[1];
//            Point center = new Point(x, y);
//            int radius = (int) circleCoordinates[2];
//
//            Imgproc.circle(mask, center, radius -5, new Scalar(255, 255, 255), -1);
//        }
        Mat fg = new Mat();
        Core.bitwise_or(crop_img, crop_img, fg, mask);
        Imgproc.cvtColor(fg, fg, Imgproc.COLOR_BGR2RGB);
        Bitmap tempdisp5 = Bitmap.createBitmap(fg.cols(), fg.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(fg, tempdisp5);


        Scalar mean_val = Core.mean(lab_image, mask);
        double Lblood   = mean_val.val[0];
        double Ablood   = mean_val.val[1];
        double Bblood   = mean_val.val[2];

//        Core.MinMaxLocResult mean_val = Core.minMaxLoc(lab_image, mask);
//        double Lblood   = mean_val.maxVal;
        Lblood = Math.round(Lblood * 1e4) / 1e4;

        Value value = new Value();
        value.Lshades = Lblood;
        value.Avalues = Ablood;
        value.Bvalues = Bblood;

        return value;
    }

    private static ValueArray getLValues(Mat image) {
        Size   size = image.size();
        double M    = size.height;
        double N    = size.width;
        //Mat crop_img = image.submat(75, (int) (M), 50, 50 + (int) (N - 500));

        Mat gray_image = new Mat(size, CvType.CV_8UC1);
        Mat smooth     = new Mat(size, CvType.CV_8UC1);
        Mat lab_image  = new Mat(size, CvType.CV_8UC1);
        Mat hsv_image  = new Mat(size, CvType.CV_8UC1);
        Mat thresh     = new Mat(size, CvType.CV_8UC1);
        Mat closing    = new Mat(size, CvType.CV_8UC1);

        Mat kernel_9x9 = new Mat(new Size(11, 11), CvType.CV_8UC1, new Scalar(1));

        Mat lower_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat upper_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat red_hue_image       = new Mat(size, CvType.CV_8UC1);

        Imgproc.cvtColor(image, gray_image, Imgproc.COLOR_BGR2GRAY);
        //Imgproc.blur(image, smooth, new Size(5, 5));
        Imgproc.cvtColor(image, lab_image, Imgproc.COLOR_BGR2Lab);
        Imgproc.cvtColor(image, hsv_image, Imgproc.COLOR_BGR2HSV);

        Core.inRange(hsv_image, new Scalar(39, 20, 30), new Scalar(100, 255, 255), lower_red_hue_range);
        Core.inRange(hsv_image, new Scalar(25, 50, 30), new Scalar(39, 255, 255), upper_red_hue_range);

        Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);

        Bitmap tempdisp1 = Bitmap.createBitmap(red_hue_image.cols(), red_hue_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(red_hue_image, tempdisp1);

//        Imgproc.threshold(gray_image, thresh, 127, 255, Imgproc.THRESH_BINARY_INV);
//
//        Bitmap tempdisp1 = Bitmap.createBitmap(thresh.cols(), thresh.rows(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(thresh, tempdisp1);

        Imgproc.morphologyEx(red_hue_image, closing, Imgproc.MORPH_CLOSE, kernel_9x9);

        tempdisp1 = Bitmap.createBitmap(closing.cols(), closing.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(closing, tempdisp1);

        Mat hierarchy = new Mat();

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(closing, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        int        maxAreaIdx = 0;
        double     maxArea    = Imgproc.contourArea(contours.get(maxAreaIdx));
        MatOfPoint maxi       = contours.get(maxAreaIdx);
        MatOfPoint temp_contour;

        Rect xywh = boundingRect(contours.get(0));
        int  diff = (int) (xywh.height / 6);
        int  x1   = xywh.x + 30;
        int  y1   = xywh.y + 20;
        int  x2   = xywh.x + xywh.width - 40;
        int  y2   = y1 + (diff - 35);

        int w = abs(x2 - x1);
        int h = abs(y2 - y1);

        Double[] Lshades = new Double[6];
        Double[] Ashades = new Double[6];
        Double[] Bshades = new Double[6];

        for (int i = 0; i < 6; i++) {

            Rect roi      = new Rect(x1, y1, w, h);
            Mat  subRects = lab_image.submat(roi);
            tempdisp1 = Bitmap.createBitmap(subRects.cols(), subRects.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(subRects, tempdisp1);

            //Mat subRects = lab_image.submat(y1, y2, x1, x2);
            //tempdisp1 = Bitmap.createBitmap(subRects.cols(), subRects.rows(), Bitmap.Config.ARGB_8888);
            // Utils.matToBitmap(subRects, tempdisp1);

            Scalar avgLab = Core.mean(subRects);
            Lshades[i] = avgLab.val[0];
            Ashades[i] = avgLab.val[1];
            Bshades[i] = avgLab.val[2];

            Imgproc.rectangle(lab_image, new Point(x1, y1), new Point(x2, y2), new Scalar(0, 255, 0), 2);

            y1 = y2 + 40;
            y2 = y1 + (diff - 40);
        }

        Bitmap tempdisp2 = Bitmap.createBitmap(lab_image.cols(), lab_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(lab_image, tempdisp2);

        Mat rgb_image = new Mat(size, CvType.CV_8UC1);
        Imgproc.cvtColor(lab_image, rgb_image, Imgproc.COLOR_Lab2RGB);
        Utils.matToBitmap(rgb_image, tempdisp2);

        ValueArray valueArray = new ValueArray();
        valueArray.Avalues = Ashades;
        valueArray.Bvalues = Bshades;
        valueArray.Lshades = Lshades;

        return valueArray;
    }

    private static int getLBlood(int canny_s, int lab_image) {
        return 0;
    }

    private static Values correction(double[] current_shades, double shades_Lmin, double shades_Lmax) {
        current_shades = around(current_shades);
        double   current_shades_max  = max(current_shades);
        double[] current_shades_norm = divideArrayByScalar(current_shades, current_shades_max);
        current_shades_norm = around(current_shades_norm);
        double current_shades_Lmin = min(current_shades_norm);

        double[] range_val = new double[current_shades_norm.length];
        Arrays.fill(range_val, 0);
        MatOfDouble mat_current_shades_norm = new MatOfDouble(current_shades_norm);
        MatOfDouble mat_range_val           = new MatOfDouble(range_val);

        Core.normalize(mat_current_shades_norm, mat_range_val, shades_Lmin, shades_Lmax, Core.NORM_MINMAX);
        range_val = mat_range_val.toArray();
        range_val = around(range_val);
        int      n                 = 5;
        double[] correction_factor = new double[current_shades_norm.length];
        for (int i = 0; i < n; i++) {
            correction_factor[i] = current_shades_norm[i] - range_val[i];
        }
        return new Values(range_val, correction_factor, current_shades_norm);

    }

    private static Values calibration() {
//        double[] clinical  = new double[]{13.3f, 15, 11.2f, 12.4f, 13.2f, 4};
//        double[] capillary = new double[]{24.5262f, 19.9915f, 27.5357f, 23.553f, 23.0764f, 44.6093f};

        double[] clinical  = new double[]{500, 10};
        double[] capillary = new double[]{19.8238, 137.4888};

        double   clinical_max  = max(clinical);
        double[] clinical_norm = divideArrayByScalar(clinical, clinical_max);
        clinical_norm = around(clinical_norm);

        double   capillary_max  = max(capillary);
        double[] capillary_norm = divideArrayByScalar(capillary, capillary_max);
        capillary_norm = around(capillary_norm);

        double[] capillary_norm_inv = decrement(capillary_norm);
        double   clinical_norm_min  = min(clinical_norm);
        double   clinical_norm_max  = max(clinical_norm);

        double[] cap_val = new double[capillary_norm_inv.length];
        Arrays.fill(cap_val, 0);
        MatOfDouble mat_capillary_norm_inv = new MatOfDouble(capillary_norm_inv);
        MatOfDouble mat_cap_val            = new MatOfDouble(cap_val);

        Core.normalize(mat_capillary_norm_inv, mat_cap_val, clinical_norm_min, clinical_norm_max, Core.NORM_MINMAX);
        double cap_val_max = max(mat_cap_val.toArray());
        double cap_val_min = min(mat_cap_val.toArray());
//        Mat mat = new Mat(1, clinical.length, CvType.CV_32F);
//        mat.put(0, 0, clinical);
//        mat.channels();


//        float[] ground_truth = new float[];
//        Imgproc.cvtColor(src, des);
//        mat.get()
//
//        float[] cap_val = new float[capillary_norm_inv.length];
//        Arrays.fill(cap_val, 0);

//        Core.normalize(mat, mat, mat);

        return new Values(cap_val_min, cap_val_max);
    }

    private static double[] divideArrayByScalar(double[] clinical, double clinical_max) {
        double[] res = new double[clinical.length];

        for (int i = 0; i < clinical.length; i++) {
            res[i] = clinical[i] / clinical_max;
        }

        return res;
    }

    private static double[] decrement(double[] capillary_norm) {
        double[] inv = new double[capillary_norm.length];

        for (int i = 0; i < capillary_norm.length; i++) {
            inv[i] = 1 - capillary_norm[i];
        }

        return inv;
    }

    private static double[] around(double[] values) {

        double[] res = new double[values.length];
        //DecimalFormat df = new DecimalFormat("#.####");

        for (int i = 0; i < values.length; i++) {
            res[i] = Math.round(values[i] * 1e4) / 1e4;
        }

        return res;
    }

    private static float estimation(double norm, double[] range_val, double correctionFac) {
        double   bloodSample = norm - correctionFac;
        double[] x           = range_val;
        int[]    y           = {500, 300, 150, 80, 30, 10};
        Values   vals        = polyFit(x, y);
        double   m           = vals.firstValue;
        double   c           = vals.secondValue;
        double   hb_val      = m * bloodSample + c;

        return (float) (hb_val);
    }

    private static Values polyFit(double x[], int y[]) {
        int n = x.length;

        double m, c, sum_x = 0, sum_y = 0, sum_xy = 0, sum_x2 = 0;

        for (int i = 0; i < n; i++) {
            sum_x += x[i];
            sum_y += y[i];
            sum_xy += x[i] * y[i];
            sum_x2 += pow(x[i], 2);
        }

        m = (n * sum_xy - sum_x * sum_y) / (n * sum_x2 - pow(sum_x, 2));
        c = (sum_y - m * sum_x) / n;

        System.out.println("m = " + m);
        System.out.println("c = " + c);

        return new Values(m, c);
    }


    private static double max(double[] numbers) {
        double max = numbers[0];

        for (double number : numbers) {
            if (number > max) {
                max = number;
            }
        }

        return max;
    }

    private static double min(double[] numbers) {
        double min = numbers[0];

        for (double number : numbers) {
            if (number < min) {
                min = number;
            }
        }

        return min;
    }

    public static void exportCsv(int[] resultValues, ValueArray valueArray, Value value1, Value value2, Value value3, float albumin) {
        try {
            String filePath = Environment.getExternalStorageDirectory() + "/" + "AlbuminCsv";

            File file = new File(filePath);
            if (!file.exists()) {
                file.mkdirs();
            }

            filePath = Environment.getExternalStorageDirectory() + "/" + "AlbuminCsv/" + System.currentTimeMillis() + ".csv";

            CSVWriter writer = new CSVWriter(new FileWriter(filePath));
            writeHeaders(writer);
            writeData(writer, resultValues, valueArray, value1, value2, value3, albumin);
            writer.close();

        } catch (Exception e) {
        }
    }

    private static void writeHeaders(CSVWriter writer) {
        ArrayList<String> headers = new ArrayList<>(16);
        headers.add("Type");
        headers.add("Albumin");
        headers.add("L");
        headers.add("A");
        headers.add("B");
        headers.add("DateTime");
        headers.add("TrueValue");

        String[] data = new String[headers.size()];
        headers.toArray(data);
        writer.writeNext(data);
    }

    private static void writeData(CSVWriter writer, int[] resultValues, ValueArray valueArray, Value value1, Value value2, Value value3, float albumin) {


        for (int i = 0; i < valueArray.Lshades.length; i++) {
            ArrayList<String> columns = new ArrayList<>(8);

            columns.add("C");
            columns.add("" + resultValues[i]);
            columns.add("" + valueArray.Lshades[i]);
            columns.add("" + valueArray.Avalues[i]);
            columns.add("" + valueArray.Bvalues[i]);
            columns.add("" + System.currentTimeMillis());

            printHeaders(writer, columns);

        }

        ArrayList<String> columns = new ArrayList<>(8);

        columns.add("T");
        columns.add("" + albumin);
        columns.add("" + value1.Lshades);
        columns.add("" + value1.Avalues);
        columns.add("" + value1.Bvalues);
        columns.add("" + System.currentTimeMillis());
        printHeaders(writer, columns);

        columns = new ArrayList<>(8);
        columns.add("T");
        columns.add("" + albumin);
        columns.add("" + value2.Lshades);
        columns.add("" + value2.Avalues);
        columns.add("" + value2.Bvalues);
        columns.add("" + System.currentTimeMillis());
        printHeaders(writer, columns);

        columns = new ArrayList<>(8);
        columns.add("T");
        columns.add("" + albumin);
        columns.add("" + value3.Lshades);
        columns.add("" + value3.Avalues);
        columns.add("" + value3.Bvalues);
        columns.add("" + System.currentTimeMillis());
        printHeaders(writer, columns);

        columns = new ArrayList<>(8);
        columns.add("Average");
        columns.add("" + albumin);
        columns.add("" + ((value1.Lshades + value2.Lshades + value3.Lshades) / 3.0));
        columns.add("" + ((value1.Avalues + value2.Avalues + value3.Avalues) / 3.0));
        columns.add("" + ((value1.Bvalues + value2.Bvalues + value3.Bvalues) / 3.0));
        columns.add("" + System.currentTimeMillis());
        printHeaders(writer, columns);
    }

    private static void printHeaders(CSVWriter writer, ArrayList<String> headers) {
        String[] data = new String[headers.size()];
        headers.toArray(data);
        writer.writeNext(data);
    }
}