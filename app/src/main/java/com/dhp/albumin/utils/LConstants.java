package com.dhp.albumin.utils;

public class LConstants {
    public static String DEFAULT_STRIP_CODE    = "CKD123456ABC";
    public static String DEFAULT_CAMPAIGN_NAME = "Bangalore Health Drive 2017";

    public static final int CODE_WRITE_PERMISSION = 101;

    public static final int DEFAULT_WAIT_DURATION = 30;

    public static final String DEFAULT_IP_ADDRESS = "192.168.0.4";

    public static final int TYPE_CALIBRATE  = 110;
    public static final int TYPE_TEST       = 111;
    public static final int TYPE_TEST_URINE = 112;

    public static final String IMAGE_PATH = ".AlbuminCaptures";
}