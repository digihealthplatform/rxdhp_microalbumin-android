package com.dhp.albumin.utils;


import android.support.v4.content.ContextCompat;

import com.dhp.albumin.LApplication;
import com.dhp.albumin.R;

import java.util.Calendar;

public class LUtils {

    public static String getFormattedDate(int date, int month, int year) {

        String formattedDate = "";

        if (date < 10) {
            formattedDate = "0" + date;
        } else {
            formattedDate = "" + date;
        }

        formattedDate = formattedDate + "-";

        if (month < 10) {
            formattedDate = formattedDate + "0" + month;
        } else {
            formattedDate = formattedDate + "" + month;
        }

        formattedDate = formattedDate + "-" + year;

        return formattedDate;
    }

    public static String getFormattedTime(int hour, int min) {
        String time;
        String am_pm;

        if (hour < 1) {
            hour = 12;
            am_pm = " AM";
        } else if (hour < 12) {
            am_pm = " AM";
        } else if (hour == 12) {
            hour = 12;
            am_pm = " PM";
        } else {
            hour = hour % 12;
            am_pm = " PM";
        }

        String minute    = Integer.toString(min);
        String hourOfDay = Integer.toString(hour);

        if (min < 10) {
            minute = "0" + minute;
        }

        time = hourOfDay + ":" + minute + am_pm;
        return time;
    }

    public static String getFormattedDateTime(long milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        return getFormattedTime(calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE))
                + ", "
                + getFormattedDate(calendar.get(Calendar.DATE),
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.YEAR));
    }

    public static int calculateAge(int date, int month, int year) {
        long daysDifference = getDifferenceInDays(date, month, year);

        if (daysDifference >= 365) {
            return (int) (daysDifference / 365);
        }

        return 1;
    }

    private static long getDifferenceInDays(int date, int month, int year) {
        Calendar currentDay = Calendar.getInstance();
        Calendar birthDay   = Calendar.getInstance();
        birthDay.set(Calendar.YEAR, year);
        birthDay.set(Calendar.MONTH, month);
        birthDay.set(Calendar.DATE, date);
        birthDay.set(Calendar.HOUR_OF_DAY, 0);
        birthDay.set(Calendar.MINUTE, 0);
        birthDay.set(Calendar.SECOND, 0);
        birthDay.set(Calendar.MILLISECOND, 0);

        return (long) (currentDay.getTimeInMillis() - birthDay.getTimeInMillis()) / (1000 * 60 * 60 * 24);
    }

    public static boolean isFutureDate(int dayOfMonth, int monthOfYear, int year) {
        if (getDifferenceInDays(dayOfMonth, monthOfYear, year) < 0) {
            return true;
        }
        return false;
    }

    public static int getResultColor(float albuminValue) {
        if (albuminValue <= 10) {
            return getColorFromResource(R.color.color_range_1);
        } else if ((albuminValue > 10) && (albuminValue <= 30)) {
            return getColorFromResource(R.color.color_range_1);
        } else if ((albuminValue > 30) && (albuminValue <= 80)) {
            return getColorFromResource(R.color.color_range_3);
        } else if ((albuminValue > 80) && (albuminValue <= 150)) {
            return getColorFromResource(R.color.color_range_3);
        } else if ((albuminValue > 150) && (albuminValue <= 300)) {
            return getColorFromResource(R.color.color_range_6);
        } else {
            return getColorFromResource(R.color.color_range_6);
        }
    }

    public static int getColorFromResource(int colorId) {
        return ContextCompat.getColor(LApplication.getContext(), colorId);
    }
}
