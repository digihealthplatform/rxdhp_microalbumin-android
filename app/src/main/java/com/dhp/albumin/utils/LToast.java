package com.dhp.albumin.utils;

import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import com.dhp.albumin.LApplication;

public class LToast {
    public static void toast(int stringId) {
        Toast.makeText(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_LONG).show();
    }

    public static void info(int stringId) {
        Toasty.info(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_LONG).show();
    }

    public static void success(int stringId) {
        Toasty.success(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_LONG).show();
    }

    public static void warning(int stringId) {
        Toasty.warning(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_LONG).show();
    }

    public static void error(int stringId) {
        Toasty.error(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_LONG).show();
    }
}